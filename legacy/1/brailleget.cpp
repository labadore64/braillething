// This file represents functions for retrieving braille values

// turns a braille character to an int array
char *braillespec_to_array(int character_id) 
{
    char* ptr = braille_def;

    for (int i = 0; i < BRAILLE_CHAR_COUNT; i++)
    {
        if (braille_char[i] == character_id)
            return ptr;
        ptr+=6;
    }

    // return null on failure
    return NULL;
}

// reads the expanded braille character and puts in compact byte encoding
// called here a braille character.
char braillespec_to_braillechar(int character_id) 
{
    // gets the array representing the braille
    char *pbraille_array = braillespec_to_array(character_id);

    if (NULL == pbraille_array)
        return static_cast<char>(-1); // all bits set (including the last two
                                      // reserved bits) means bad character

    char out = 0;
    for (int j = BRAILLE_CELL_COUNT - 1; j >= 0; --j)
    {
        out <<= 1;
        out |= pbraille_array[j];
    }
    
    return out;
}

