// contains functions for translating between UTF8, UTF32, and braille8

static inline int _utf_len(int i)
{
    static int const tab[32] = {
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,6,6,1,1,
    };
    if (i < 192) return 1;
    if (i < 224) return 2;
    /* look ma, no hands! */
    return tab[i - 224];
}

static inline int _utf_slen(braille_utfchar8_t c)
{
    return _utf_len(static_cast<int>(static_cast<unsigned char>(c)));
}

static braille_utfchar32_t const _utf_mask[6] = {
    0x7F,
    0x1F,
    0x0F,
    0x07,
    0x03,
    0x01
};

/* utf_clen
 * params:
 *   braille_utfchar8_t c - the first byte of a UTF-8 code point
 * return:
 *   int                  - the length of the UTF-8 encoding of a code point
 *                          beginning with c
 * */
int utf_clen(braille_utfchar8_t c)
{
    return (int)_utf_slen(c);
}

/* utf_8_to_32
 * params:
 *   braille_utfchar32_t *pout - code point output address
 *   braille_utfchar8_t    *pc - char array input address
 * return:
 *   int                       - length of the decoded code point
 * */
int utf_8_to_32(braille_utfchar32_t *pout, const braille_utfchar8_t *pc)
{
    int i, len;
    braille_utfchar32_t result;

    if (*pc == 0)
        return static_cast<int>(BRAILLE_EOF32); /* 0xFFFFFFFF */

    len = _utf_slen(*pc);
    result = (braille_utfchar32_t)pc[0] & _utf_mask[len-1];

    for (i = 1; i < len; ++i)
    {
        /* malformed character interrupted by end of string */
        if (pc[i] == '\0')
            return static_cast<int>(BRAILLE_EOF32);

        result <<= 6;
        result |= pc[i] & 0x3F;
    }

    *pout = result;
    return len;
}

/* utf_8_to_32
 * params:
 *   braille_utfchar32_t *pout - char array output address (must be six bytes)
 *   braille_utfchar8_t     u  - code point
 * return:
 *   int                       - length of the encoded code point
 * */
int utf_32_to_8(braille_utfchar8_t *pout, braille_utfchar32_t u)
{
    int i, len;
    braille_utfchar32_t first;

    if (u < 0x80)
    {
        first = 0;
        len = 1;
    }
    else if (u < 0x800)
    {
        first = 0xC0;
        len = 2;
    }
    else if (u < 0x10000)
    {
        first = 0xE0;
        len = 3;
    }
    else if (u < 0x200000)
    {
        first = 0xF0;
        len = 4;
    }
    else if (u < 0x4000000)
    {
        first = 0xF8;
        len = 5;
    }
    else
    {
        first = 0xFC;
        len = 6;
    }

    for (i = len - 1; i > 0; --i)
    {
        pout[i] = (u & 0x3F) | 0x80;
        u >>= 6;
    }
    pout[0] = u | first;

    return len;
}

braille_pinenc8_t utf_32_to_braille8(braille_utfchar32_t ch) 
{
    braille_pinenc8_t *pbraille_array = &braille_def[0];
    int i = 0;

    while (0 != *pbraille_array && i < BRAILLE_CHAR_COUNT)
    {
        if (braille_char[i] == ch)
            return *pbraille_array;

        ++pbraille_array;
        ++i;
    }

    return BRAILLE_BAD;
}

int utf_8_to_braille8(braille_pinenc8_t *pout, braille_utfchar8_t const *pc) 
{
    braille_utfchar32_t c32;
    int len;
   
    len = utf_8_to_32(&c32, pc);
    *pout = utf_32_to_braille8(c32);

    return len;
}

