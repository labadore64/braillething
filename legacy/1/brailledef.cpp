// number of characters in the encoding table
#define BRAILLE_CHAR_COUNT 27

// braille character dimensions in cells
#define BRAILLE_COL_COUNT 2
#define BRAILLE_ROW_COUNT 3

// number of braille cells per braille character
#define BRAILLE_CELL_COUNT (BRAILLE_COL_COUNT*BRAILLE_ROW_COUNT)

// bad braille char, all bits set
#define BRAILLE_BAD      (static_cast<char>(-1))

// mask for checking if the last bit is set (indicating bad char)
#define BRAILLE_BAD_MASK 0x80

// end of data indicator for utf32
#define BRAILLE_EOF32    (static_cast<unsigned long>(-1))

// pinenc8 def
#define BRAILLE8_PACK(a, b, c, d, e, f) \
    (a | (b << 1) | (c << 2) | (d << 3) | (e << 4) | (f << 5) | (1 << 6))

// typedefs for chars
typedef unsigned long braille_utfchar32_t; // UTF32 input character type
typedef          char braille_utfchar8_t;  // UTF8 input character type
typedef unsigned char braille_pinenc8_t;   // pin encoding type

// Represents the character encoding.
// Just setting this to 0 for now, will be
// used later.
static unsigned char encoding = 0;

// Represents the braille cell characters. 0 terminated for safety.
static braille_utfchar32_t braille_char[BRAILLE_CHAR_COUNT+1] = {
    ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    0
};

// Represents the cell values. 0 terminated for safety.
// 0 = off, 1 = on
// values are represented in this format (row major)
// 0 1
// 2 3
// 4 5
static braille_pinenc8_t braille_def[BRAILLE_CHAR_COUNT + 1] = {
    BRAILLE8_PACK(0, 0, 0, 0, 0, 0),  // space
    BRAILLE8_PACK(1, 0, 0, 0, 0, 0),  // a
    BRAILLE8_PACK(1, 0, 1, 0, 0, 0),  // b
    BRAILLE8_PACK(1, 1, 0, 0, 0, 0),  // c
    BRAILLE8_PACK(1, 1, 0, 1, 0, 0),  // d
    BRAILLE8_PACK(1, 0, 0, 1, 0, 0),  // e
    BRAILLE8_PACK(1, 1, 1, 0, 0, 0),  // f
    BRAILLE8_PACK(1, 1, 1, 1, 0, 0),  // g
    BRAILLE8_PACK(1, 0, 1, 1, 0, 0),  // h
    BRAILLE8_PACK(0, 1, 1, 0, 0, 0),  // i
    BRAILLE8_PACK(0, 1, 1, 1, 0, 0),  // j
    BRAILLE8_PACK(1, 0, 0, 0, 1, 0),  // k
    BRAILLE8_PACK(1, 0, 1, 0, 1, 0),  // l
    BRAILLE8_PACK(1, 1, 0, 0, 1, 0),  // m
    BRAILLE8_PACK(1, 1, 0, 1, 1, 0),  // n
    BRAILLE8_PACK(1, 0, 0, 1, 1, 0),  // o
    BRAILLE8_PACK(1, 1, 1, 0, 1, 0),  // p
    BRAILLE8_PACK(1, 1, 1, 1, 1, 0),  // q
    BRAILLE8_PACK(1, 0, 1, 1, 1, 0),  // r
    BRAILLE8_PACK(0, 1, 1, 0, 1, 0),  // s
    BRAILLE8_PACK(0, 1, 1, 1, 1, 0),  // t
    BRAILLE8_PACK(1, 0, 0, 0, 1, 1),  // u
    BRAILLE8_PACK(1, 0, 1, 0, 1, 1),  // v
    BRAILLE8_PACK(0, 1, 1, 1, 0, 1),  // w
    BRAILLE8_PACK(1, 1, 0, 0, 1, 1),  // x
    BRAILLE8_PACK(1, 1, 0, 1, 1, 1),  // y
    BRAILLE8_PACK(1, 0, 0, 1, 1, 1),  // z
    0
};

