// These are functions that are used for debugging output to the terminal.

// write braille characters to a stream.
void braille8_to_ostream(
    std::ostream &moss, braille_pinenc8_t const *pcbraille_in) 
{
    // pointer to the encoded braille characters
    braille_pinenc8_t const *pcbraille;
    // loop indices
    int i, j;
    for (i = 0; i < BRAILLE_ROW_COUNT; ++i)
    {
        for (pcbraille = pcbraille_in; *pcbraille != '\0'; ++pcbraille)
        {
            // get the current character
            braille_pinenc8_t cbraille = *pcbraille;

            if (cbraille & BRAILLE_BAD_MASK)
                // if either the guard bits are set, it's bad so just put
                // question marks
                moss << "??";
            else
            {
                // skip the first i rows
                cbraille >>= i * BRAILLE_COL_COUNT;
                for (j = 0; j < BRAILLE_COL_COUNT; ++j)
                {
                    // insert an asterisk for selected, and a dash for not
                    // selected
                    char out = (cbraille & 1) ? '*' : '-';
                    moss << out;
                    cbraille >>= 1;
                }
            }
            // lookahead by 1 so we don't but a space at the end, but a line
            // ending.
            if (pcbraille[1] != '\0')
                moss << ' ';
            else
                moss << std::endl;
        }
    }
}

void utf8_labels_to_ostream(std::ostream &moss, braille_utfchar8_t const *pstr)
{
    char lastchar[7];
    while (*pstr != '\0') // while we haven't encountered a zero
    {
        // get the braille character
        braille_pinenc8_t cb;
        int adv = utf_8_to_braille8(&cb, pstr);
        std::memcpy(&lastchar[0], pstr, adv);
        lastchar[adv] = '\0';
        pstr += adv;

        // put title of the character, or questionmark if it's bad
        moss << ((cb & BRAILLE_BAD_MASK) ? "?" : &lastchar[0]);

        // put spacing
        for (int i = 0; i < BRAILLE_COL_COUNT - 1; ++i) moss << ' ';
        if (pstr[0] != '\0')
            moss << ' ';
    }
    // insert a newline at the end of the label row
    moss << std::endl;
}

void utf32_labels_to_ostream(std::ostream &moss, braille_utfchar32_t const *pstr)
{
    braille_utfchar32_t ch = 0;
    while ((ch = *pstr++) != 0) // while we haven't encountered a zero
    {
        // get the braille character
        char cb = utf_32_to_braille8(ch);

        // put title of the character, or questionmark if it's bad
        moss << ((cb & BRAILLE_BAD_MASK) ? '?' : static_cast<char>(ch));

        // put spacing
        for (int i = 0; i < BRAILLE_COL_COUNT - 1; ++i) moss << ' ';
        if (pstr[0] != 0)
            moss << ' ';
    }
    // insert a newline at the end of the label row
    moss << std::endl;
}

// write utf32 chacters to a stream
void utf32_to_ostream(std::ostream &moss, braille_utfchar32_t const *pstr) 
{
    // calculate the length of the pstr array
    braille_utfchar32_t const *pc32 = pstr;
    while (*pc32++ != '\0');
    int len = static_cast<int>(pc32 - pstr);

    // make room an the stack for the braille chars plus the terminating zero
    braille_pinenc8_t *pcbraille =
        static_cast<braille_pinenc8_t *>(alloca(len + 1)); 
    
    braille_utfchar32_t ch = 0;
    braille_pinenc8_t *pcb = pcbraille;
    while ((ch = *pstr++) != 0) // while we haven't encountered a zero
    {
        // get the braille character
        char cb = utf_32_to_braille8(ch);

        // put title of the character, or questionmark if it's bad
        moss << ((cb & BRAILLE_BAD_MASK) ? '?' : static_cast<char>(ch));

        // put spacing
        for (int i = 0; i < BRAILLE_COL_COUNT - 1; ++i) moss << ' ';
        if (pstr[0] != 0)
            moss << ' ';

        // write encoded braille character in the intermediate string.
        *pcb++ = cb;
    }
    // insert a newline at the end of the label row
    moss << std::endl;

    // make sure the braille string is null terminated
    *pcb = '\0';

    // write braillechars to stream
    braille8_to_ostream(moss, pcbraille);
}

