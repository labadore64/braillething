#include <iostream> // standard terminal I/O
#include <sstream>  // construction of strings
#include <alloca.h> // dynamic stack allocation
#include <cstring>

#include "brailledef.cpp" // definitions of braille values
#include "brailleenc.cpp" // handle per character transcoding
#include "braillechunk.cpp" // handle stream transcoding

#include "brailleterm.cpp" // terminal output for debugging

// program entry point
int main(void) 
{
    std::cout << "test direct stream dump" << std::endl;

    // write straight to standard output
    utf32_to_ostream(std::cout, braille_char);
    
    std::cout << "test stringstream dump" << std::endl;

    // write to a string stream, then get the string and write that to output
    std::ostringstream ss;
    utf32_to_ostream(ss, braille_char);
    std::string s = ss.str();
    std::cout << s;

    std::cout << "test streaming messages" << std::endl;

    char const *test_message1 = "hello world";
    char const *test_message2 = "\u03B1\u03B2\u03B3 alpha beta and gamma are more than one byte apiece in utf8 but we're smart and we know they're one codepoint apiece. :)";

    /* we'll do 20 codepoints per row for this test */
    int const ROWSIZE = 20;
    braille_utfchar8_t inbuf[ROWSIZE*6+1];
    braille_pinenc8_t outbuf[ROWSIZE+1];
    char const *pc8;
    braille_pinenc8_t *pcb;
    int nout, nin;

    std::cout << "test hello world message" << std::endl;

    /* chunk off the rows */
    pcb = &outbuf[0];
    pc8 = test_message1;
    nin = std::strlen(test_message1);
    while (*pc8 != '\0')
    {
        nout = ROWSIZE;
        int advance = utf_8_to_braille8_stream(pcb, pc8, nin, &nout);
        outbuf[nout] = '\0'; /* null terminate the output buffer at the actual
                              * end position of the output */

        /* copy the same part chunked off for the current row to the 
         * buffer we'll use for labels */
        std::memcpy(&inbuf[0], &pc8[0], advance);
        inbuf[advance] = '\0';

        utf8_labels_to_ostream(std::cout, &inbuf[0]);
        braille8_to_ostream(std::cout, &outbuf[0]);
        std::cout << std::endl;

        nin -= advance; /* decrement the amount of input characters left */
        pc8 += advance; /* increment the input pointer */
    }

    std::cout << "test alpha beta gamma message" << std::endl;

    /* chunk off the rows */
    pcb = &outbuf[0];
    pc8 = test_message2;
    nin = std::strlen(test_message2);
    while (*pc8 != '\0')
    {
        nout = ROWSIZE;
        int advance = utf_8_to_braille8_stream(pcb, pc8, nin, &nout);
        outbuf[nout] = '\0'; /* null terminate the output buffer at the actual
                              * end position of the output */

        /* copy the same part chunked off for the current row to the 
         * buffer we'll use for labels */
        std::memcpy(&inbuf[0], &pc8[0], advance);
        inbuf[advance] = '\0';

        utf8_labels_to_ostream(std::cout, &inbuf[0]);
        braille8_to_ostream(std::cout, &outbuf[0]);
        std::cout << std::endl;

        nin -= advance; /* decrement the amount of input characters left */
        pc8 += advance; /* increment the input pointer */
    }

    return 0;
}

