// This file contains functions for streaming transcoding of strings to braille
// chunks

// returns how much to advance the input stream in bytes
// pout is the pointer to the output buffer
// pinp is the pointer to the input buffer at the current stream position
// ninp is the number of input bytes available, *not symbols*.
// nout is a pointer to the desired number of output symbols. this will be
// overwritten by the actual number of output symbols
int utf_8_to_braille8_stream(braille_pinenc8_t *pout,
    braille_utfchar8_t const *pinp, int ninp, int *nout)
{
    int nout_target, i, len;
    braille_utfchar8_t const *pcurs_in;
    braille_pinenc8_t *pcurs_out;
    braille_pinenc8_t cb;

    nout_target = *nout;
    i = 0;
    pcurs_in = pinp;
    pcurs_out = pout;
    *nout = 0;

    while ('\0' != *pcurs_in && i < ninp && *nout < nout_target)
    {
        len = utf_8_to_braille8(&cb, pcurs_in);
        if (len == static_cast<int>(BRAILLE_EOF32))
            break;

        *pcurs_out++ = cb;
        pcurs_in += len;
        i += len;
        ++(*nout);
    }

    return static_cast<int>(pcurs_in - pinp);
}

// returns how much to advance the input stream in bytes
// pout is the pointer to the output buffer
// pinp is the pointer to the input buffer at the current stream position
// ninp is the number of input bytes available, *not symbols*.
// nout is a pointer to the desired number of output symbols. this will be
// overwritten by the actual number of output symbols
int utf_32_to_braille8_stream(braille_pinenc8_t *pout,
    braille_utfchar32_t const *pinp, int ninp, int *nout)
{
    int nout_target, i;
    braille_utfchar32_t const *pcurs_in;
    braille_pinenc8_t *pcurs_out;

    nout_target = *nout;
    i = 0;
    pcurs_in = pinp;
    pcurs_out = pout;
    *nout = 0;

    while (0U != *pcurs_in && i < ninp && *nout < nout_target)
    {
        *pcurs_out++ = utf_32_to_braille8(*pcurs_in++);
        ++i;
        ++(*nout);
    }

    return static_cast<int>(pcurs_in - pinp);
}

