# Copyright 2020 Roger Merryfield
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# proj: Braille8 Display Reference Prototype
# file: braille8tab.py
# date: 2020-12-16
# desc: Braille8 transcoder driver table generator.
# .

import math, sys, string, hashlib, yaml
from pathlib import Path
from functools import partial
from io import StringIO, DEFAULT_BUFFER_SIZE

import codegen

try:
    from yaml import CLoader as YamlLoader, CDumper as YamlDumper
except ImportError:
    from yaml import Loader as YamlLoader, Dumper as YamlDumper

def yaml_load(*args, **kwargs):
    return yaml.load(*args, **kwargs, Loader=YamlLoader)

def yaml_dump(*args, **kwargs):
    return yaml.dump(*args, **kwargs, Dumper=YamlDumper)

def file_byte_iterator(path):
    """given a path, return an iterator over the file that lazily loads the
    file and returns each byte. the iterator will always yield one value,
    because it will first stat file and return the resultant structure.
    """
    path = Path(path)
    yield path.stat()
    with path.open('rb') as f:
        reader = partial(f.read1, DEFAULT_BUFFER_SIZE)
        file_iterator = iter(reader, b'')
        for chunk in file_iterator:
            yield from chunk

def read_defs(filename):
    reader = file_byte_iterator(filename)
    stat = next(reader)

    struct = yaml_load(bytes(reader))
    reader = file_byte_iterator(filename)
    next(reader)

    checksum = hashlib.sha256(bytes(reader)).hexdigest()

    return stat, struct, checksum

def extract_top_level(struct):
    return extract_subsections('braille8 YAML Specification', 'braille8-', struct)

def extract_rules(section):
    return extract_subsections('braille8 Transcoder Rules', '', section)

def trie_new():
    return [dict(), None]

def trie_inject(trie, key, value):
    if len(key) == 0:
        if trie[1] is not None:
            raise ValueError("Cannot inject same key twice!")

        trie[1] = value
    else:
        first, tail = key[0], key[1:]
        if first not in trie[0]:
            trie[0][first] = trie_new()
        
        return trie_inject(trie[0][first], tail, value)

def write_cdefs_meta(filename, stat, checksum, sections, out):
    specs = sections['specifications'][0]
    drivers = sections['drivers']

    driver_list = []
    for i, drv in enumerate(drivers):
        driver_list.append(f"""driver ({i}) {repr(drv['name'])}:
  @                 :
  revision:         {drv['revision']}
  date:             {drv['date']}
  authors:          {', '.join(drv['authors'])}
  input-encodings:  {', '.join(drv['input-encodings'])}
  braille-encoding: {drv['braille-encoding']}""")

    newline = '\n'
    codegen.write_multiline_comment(0, f"""braille8 Transcoder Tables
meta:
  @            :
  input bytes: {stat.st_size}
  checksum:    {checksum}
stat:
  @                   :
  filename:           {filename}
  size:               {stat.st_size}
  blocks:             {stat.st_blocks}
  inode or fileindex: {stat.st_ino}
  device:             {stat.st_dev}
  hardlink count:     {stat.st_nlink}
  user id:            {stat.st_uid}
  group id:           {stat.st_gid}
  accessed (ns):      {stat.st_atime_ns}
  modified (ns):      {stat.st_mtime_ns}
  meta (ns):          {stat.st_ctime_ns}
specification meta:
  @                 :
  name:             {specs['name']}
  revision:         {specs['revision']}
  date:             {specs['date']}
  authors:          {', '.join(specs['authors'])}
  braille-encoding: {specs['braille-encoding']}
  braille-subsets:  {', '.join(specs['braille-subsets'])}
drivers:
  @      :
  names: {', '.join(drv['name'] for drv in drivers)}
  count: {len(drivers)}
{newline.join(driver_list)}""", out, math.inf)

def extract_subsections(sectionname, toplevelprefix, sections):
    subsections = dict()

    prefixlen = len(toplevelprefix)
    for subsection in sections:
        keys = subsection.keys()
        if len(keys) != 1:
            raise RuntimeError("Malformed braille8 specification file. Each subsection of a section must have one dictionary entry, whose key specifies its title.")
        for k in keys:
            if toplevelprefix != k[:prefixlen]:
                raise RuntimeError(f"Malformed braille8 specification file. The title of each subsection in the {repr(sectionname)} subsection must be prefixed with {repr(toplevelprefix)}.")
            subsections[k[prefixlen:]] = subsection[k]

    return subsections

if __name__ == "__main__":
    try:
        filename = sys.argv[1]
    except:
        filename = "../spec/braille8.yml"

    stat, struct, checksum = read_defs(filename)
    sections = extract_top_level(struct)

    write_cdefs_meta("spec/braille8.yml", stat, checksum, sections, sys.stdout)   

    rules_sections = extract_rules(sections['rules'])
    print(rules_sections.keys())

    br8_maxseq = 0

    # get the max size of a braille8 emission sequence
    for inp in rules_sections['input']:
        br8_maxseq = max(len(inp['pins']), br8_maxseq)

    sequences = dict()
    label_names = []
    labels = dict()
    state_names = []
    state_set = dict()
    state_clear = dict()

    def inject_key_seq(names, dct, key, inp):
        if key in inp:
            k = inp[key]
            for label in (k if isinstance(k, list) else (k,)):
                if names is not None:
                    no = len(names)
                    names.append(label)
                    label = no
                dct[label] = inp

    # input sequence and label registration
    for inp in rules_sections['input']:
        inject_key_seq(None, sequences, 'sequence', inp)
        inject_key_seq(label_names, labels, 'label', inp)

    # state control table
    for st in rules_sections['state-control']:
        name = st['name']
        state_names.append(name)
        state_set[name] = st['set']
        state_clear[name] = st['clear']

    print(sequences.keys())
    print(labels.keys())
    print(state_names)

    seq_trie = trie_new()

    for k, q in sequences.items():
        trie_inject(seq_trie, k, q)

    # dump off the sequences trie  
    def recurse_dump_trie(trie, N, head):
        sortedkeys = list(sorted(trie[0].keys()))

        thisN = N[0]
        N[0] += 1

        subN = []
        for k in sortedkeys:
            subN.append(recurse_dump_trie(trie[0][k], N, head+k))

        codegen.write_multiline_comment(0, f"Sequence trie node {thisN} subtrie table.", sys.stdout, 80)

        kvout = StringIO()
        print('{', codegen.numarray_element('hex', 32, False, 0),
                end=', ', file=kvout)

        if trie[1] is None:
            print("NULL", end=' }', file=kvout)
        else:
            print(f"(void const *)&seqmatch{thisN}",
                    end=' }', file=kvout)

        if trie[1] is not None:
            print(f"static struct brmatch const seqmatch{thisN} = ",
                    end='', file=sys.stdout)
            codegen.write_embed_array(0, 2, [
                '"'+''.join(('\\u'+f"{ord(c):>08}" if ord(c) > 127 or c == '"' else c) for c in head)+'"', '{0}'
                ], sys.stdout, 80)

        kvs = [kvout.getvalue()]
        for n, k in zip(subN, sortedkeys):
            kvout = StringIO()
            print('{', codegen.numarray_element('hex', 32, False, ord(k)),
                    end=', ', file=kvout)
            print(f"(void const *)&{codegen.array_data_varname(f'seqnode{n}')}[0]",
                    end=' }', file=kvout)
            kvs.append(kvout.getvalue())

        codegen.write_const_array(0, 2, 'struct brtrienode', f'seqnode{thisN}', len(sortedkeys), kvs, sys.stdout, 80)
        
        #if trie[1] is not None:
        #    print('    DATA', trie[1])

        return thisN

    print(f"""
struct brtrienode
{{
  BRINT32 k;
  void const *psub;
}};

struct brmatch
{{
  char const *smatchinp;
  unsigned BRINT8 brseq[{br8_maxseq + 1}];
}};
""", file=sys.stdout)
        
    N = [0]
    recurse_dump_trie(seq_trie, N, '')

    #print(yaml_dump(sections['drivers']))
    #print(yaml_dump(sections['specifications']))
    #print(yaml_dump(sections['rules']))
    #for subsec in sections['rules']:
    #    for k in subsec.keys():
    #        print(k)

