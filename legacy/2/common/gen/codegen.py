# Copyright 2020 Roger Merryfield
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# proj: Braille8 Display Reference Prototype
# file: codegen.py
# date: 2020-12-16
# desc: Code generator library used by the braille8tab program to bake the
#       transcoder tables.
# .

def tokenize(a, sep = ' \t\v\n\r'):
    ws = []
    tok = []
    for c in a:
        if c not in sep:
            if ws:
                yield ''.join(tok), ''.join(ws)
                while tok: tok.pop()
                while ws: ws.pop()
                accum = 0
            tok.append(c)
        else:
            ws.append(c)
    if tok or ws:
        yield ''.join(tok), ''.join(ws)

_numarray_sfun = {
    'hex': (lambda x: hex(x)[2:]),
    'oct': (lambda x: oct(x)[2:]),
    'dec': str
}

_numarray_pack_pfun = {
    'hex': (lambda d, s: '0x' + s),
    'oct': (lambda d, s: '0' + s),
    'dec': (lambda d, s: str(s))
}

_numarray_align_pfun = {
    'hex': (lambda d, s: f'0x{s:>0{d}}'),
    'oct': (lambda d, s: f'0{s:>0{d}}'),
    'dec': (lambda d, s: ' '*(d - len(s)) + s)
}

def write_multiline_decorated(first, mid, end, tab, inp, out, maxwidth):
    if len(first) != len(mid):
        raise ValueError("Length of the first line and mid line decorators must be the same.")

    decolen = len(first)
    width_accum = tab + decolen
    linestart = True
    tabspace = ' '*tab
    newline = tabspace + mid
    print(tabspace + first, end='', file=out)
    for tok, ws in tokenize(inp):
        add_width = len(tok)
        if width_accum + add_width > maxwidth:
            print(file=out)
            print(newline, end='', file=out)
            width_accum = tab + decolen
            linestart = True
        print(tok, end='', file=out)
        width_accum += add_width
        if add_width:
            linestart = False

        for s, nl in tokenize(ws, '\n'):
            print(s, end='', file=out)
            add_width = len(s)
            numlines = len(nl)
            if width_accum + add_width > maxwidth:
                print(file=out)
                print(newline, end='', file=out)
                width_accum = tab + decolen
                numlines -= 1
                linestart = True
            else:
                width_accum += add_width
                if add_width:
                    linestart = False
            for i in range(numlines):
                print(file=out)
                print(newline, end='', file=out)
                width_accum = tab + decolen
                linestart = True

    if width_accum + len(end) + 1 < maxwidth:
        if len(end) != 0 and not linestart:
            print(' ', end='', file=out)
        print(end, file=out)
    elif len(end) != 0:
        print(file=out)
        print(newline + end, file=out)
    else:
        print(file=out)

def write_multiline_comment(tab, inp, out, maxwidth):
    write_multiline_decorated('/* ', ' * ', '*/', tab, inp, out, maxwidth)

def write_cxxstyle_comment(tab, inp, out, maxwidth):
    write_multiline_decorated('// ', '// ', '', tab, inp, out, maxwidth)

def write_array_elements(tab, inp, out, maxwidth):
    print_comma = False
    width_accum = tab
    tabspace = ' '*tab
    for value in inp:
        s = str(value)
        add_width = len(s)
        if print_comma: add_width += 2
        if width_accum + add_width >= maxwidth:
            if print_comma:
                print(',', file=out)
            print(tabspace, end='', file=out)
            print_comma = False
            add_width -= 2
            width_accum = tab
        if print_comma:
            print(', ', end='', file=out)
        print(s, end='', file=out)
        width_accum += add_width
        print_comma = True
    print(file=out)

def _prepare_numarray_transducer(mode, bitwidth, pack):
    global _numarray_sfun, _numarray_pack_pfun, _numarray_align_pfun
    _numarray_pfun = _numarray_pack_pfun if pack else _numarray_align_pfun

    if mode == 'hex':
        d = bitwidth >> 2
        if bitwidth & 3 != 0: d += 1
    elif mode == 'oct':
        d = bitwidth // 3
        if bitwidth % 3 != 0: d += 1
    elif mode == 'dec':
        halfmax = 2**(bitwidth-1) - 1
        d = int(math.ceil(math.log10(halfmax + halfmax + 1)))
    else:
        raise ValueError("mode must be 'hex', 'oct', or 'dec'.")

    sfun = _numarray_sfun[mode]
    pfun = _numarray_pfun[mode]

    return sfun, pfun, d

def numarray_element(mode, bitwidth, pack, inp1):
    sfun, pfun, d = _prepare_numarray_transducer(mode, bitwidth, pack)
    return pfun(d, sfun(inp1))

def numarray_elements(mode, bitwidth, pack, inp):
    sfun, pfun, d = _prepare_numarray_transducer(mode, bitwidth, pack)
    for value in inp:
        yield pfun(d, sfun(value))

def _write_array_tail(tab, tabinner, inp, out, maxwidth):
    print(' '*tabinner, end='', file=out)
    write_array_elements(tabinner, inp, out, maxwidth)
    print((' '*tab)+'};', file=out)

def array_data_varname(name):
    return f"{name}_data"

def array_size_varname(name):
    return f"{name}_size"
        
def write_const_array(tab, tabinner, typ, name, sz, inp, out, maxwidth, isstatic=True):
    S = 'static ' if isstatic else ''
    print(f"{' '*tab}{S}unsigned long const {name}_size = {sz};", file=out)
    print(f"{' '*tab}{S}{typ} const {name}_data[{sz}] = {{", file=out)
    _write_array_tail(tab, tabinner, inp, out, maxwidth)

def write_embed_array(tab, tabinner, inp, out, maxwidth):
    print((' '*tab)+'{', file=out)
    _write_array_tail(tab, tabinner, inp, out, maxwidth)

# a lil test :)
if __name__ == '__main__':
    import sys

    loremipsum = """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris accumsan, felis efficitur volutpat iaculis, quam lacus efficitur tellus, sit amet tincidunt massa tellus sit amet lectus. In egestas ultricies ex, vel aliquam risus volutpat at. Sed tincidunt blandit tellus, sit amet tempor nulla maximus sit amet. Sed turpis nibh, pharetra et risus at, fringilla aliquam mauris. Vivamus feugiat lobortis orci, ac condimentum mauris ultricies ac. Integer a ex nulla. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque hendrerit ipsum elit, at condimentum massa congue ac. Aenean non dapibus ex, quis pretium dolor. Donec id quam a neque sodales aliquam et quis lorem. Donec eu elementum mi.

Cras tristique volutpat ex et accumsan. Aenean tempus orci vel ultricies tincidunt. Quisque convallis laoreet finibus. Maecenas sit amet interdum lacus. Vestibulum sit amet vestibulum tellus. Curabitur aliquam est at sem feugiat, a tempor est ultricies. Morbi a dui vitae enim fermentum vehicula vitae nec eros. Phasellus lacinia, dolor ut tempus pulvinar, libero massa pellentesque elit, non feugiat neque justo nec sem. Nullam ac arcu imperdiet, venenatis ligula sed, aliquam sapien. Pellentesque at eleifend metus, nec interdum ipsum. Vestibulum ut efficitur turpis. Morbi orci enim, facilisis eu placerat vel, molestie ac nunc.

Vivamus imperdiet, mi nec maximus sollicitudin, dui ipsum mollis erat, vitae placerat leo tellus nec tellus. Duis porttitor libero non est molestie porta. Fusce varius tortor nec enim vulputate, at maximus lectus fermentum. Nullam blandit enim vel porta consectetur. Nulla ut magna nec tortor rutrum mattis. Vivamus ornare vulputate lectus et ultrices. Vestibulum feugiat arcu vitae urna pretium, a luctus eros scelerisque. Vivamus semper tincidunt vehicula. Donec vitae ante quam. Integer egestas massa vitae orci imperdiet, ut efficitur arcu dignissim.

Pellentesque eu eros velit. Duis elit arcu, fringilla eget laoreet et, posuere vitae turpis. Nullam et semper elit, sit amet feugiat metus. Integer vitae porttitor est. Donec quis est id libero posuere finibus. Ut in sodales erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque consectetur commodo volutpat. Donec et magna eget mi finibus finibus id eget enim. Praesent fermentum, eros in condimentum dictum, dui purus finibus tellus, et sollicitudin eros ante at quam. Donec id enim quam. Aliquam erat volutpat. Donec non nunc non tellus vulputate tempus ut eget quam.

Fusce placerat neque metus, in tincidunt neque sodales sed. In urna ex, vehicula ac egestas non, lacinia in elit. Praesent facilisis libero eu enim vulputate scelerisque. Sed suscipit mi sit amet molestie mollis. Donec varius rhoncus sapien consequat aliquet. Nunc id velit ligula. Etiam vulputate leo massa, vel sagittis arcu pretium at. Nunc non elit odio. Duis lobortis lacus nec arcu dapibus vulputate. Proin vitae mi ac elit tempus faucibus et a lacus. Maecenas eu ex nulla. Vestibulum a ipsum non nibh dictum pretium. Morbi vulputate justo at urna vestibulum euismod. Donec non fermentum augue."""

    write_multiline_comment(2, loremipsum, sys.stdout, 80)
    write_cxxstyle_comment(2, loremipsum, sys.stdout, 80)

    
