/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 * proj: Braille8 Display Reference Prototype
 * file: brenc.cxx
 * date: 2020-12-12
 * desc: Implementation of transcoding subroutines.
 *
 * */

#include "brdefs.hxx"

/* utf8 character length projection with abridged table driven approach. */
static inline int _utf_len(int i)
{
  static int const tab[32] =
  {
    3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,6,6,1,1,
  };

  if (i < 192) return 1;
  if (i < 224) return 2;
  /* look ma, no hands! */
  return tab[i - 224];
}

/* using two static casts to explicitly manipulate the sign on two's compliment
 * machinery. */
static inline int _utf_slen(utfchar8_t c)
{
  return _utf_len(static_cast<int>(static_cast<unsigned char>(c)));
}

/* table for utf8 code sequence masks */
static utfchar32_t const _utf_mask[6] = {
  0x7F,
  0x1F,
  0x0F,
  0x07,
  0x03,
  0x01
};

/* utf_clen
 * params:
 *   utfchar8_t c - the first byte of a UTF-8 code point
 * return:
 *   int          - the length of the UTF-8 encoding of a code point
 *                  beginning with c
 * */
int utf_clen(utfchar8_t c)
{
  return (int)_utf_slen(c);
}

/* utf_8_to_32
 * params:
 *   utfchar32_t *pout - code point output address
 *   utfchar8_t    *pc - char array input address
 * return:
 *   int               - length of the decoded code point
 * */
int utf_8_to_32(utfchar32_t *pout, utfchar8_t const *pc)
{
  int i, len;
  utfchar32_t result;

  if (*pc == 0)
    return static_cast<int>(BRAILLE_UTFBAD); /* 0xFFFFFFFF */

  len = _utf_slen(*pc);
  result = (utfchar32_t)pc[0] & _utf_mask[len-1];

  for (i = 1; i < len; ++i)
  {
    /* malformed character interrupted by end of string */
    if (pc[i] == '\0')
      return static_cast<int>(BRAILLE_UTFBAD);

    result <<= 6;
    result |= pc[i] & 0x3F;
  }

  *pout = result;
  return len;
}

/* utf_8_to_32
 * params:
 *   utfchar32_t *pout - char array output address (must be six bytes)
 *   utfchar8_t     u  - code point
 * return:
 *   int               - length of the encoded code point
 * */
int utf_32_to_8(utfchar8_t *pout, utfchar32_t u)
{
  int i, len;
  utfchar32_t first;

  if (u < 0x80)
  {
    first = 0;
    len = 1;
  }
  else if (u < 0x800)
  {
    first = 0xC0;
    len = 2;
  }
  else if (u < 0x10000)
  {
    first = 0xE0;
    len = 3;
  }
  else if (u < 0x200000)
  {
    first = 0xF0;
    len = 4;
  }
  else if (u < 0x4000000)
  {
    first = 0xF8;
    len = 5;
  }
  else
  {
    first = 0xFC;
    len = 6;
  }

  for (i = len - 1; i > 0; --i)
  {
    pout[i] = (u & 0x3F) | 0x80;
    u >>= 6;
  }
  pout[0] = u | first;

  return len;
}

