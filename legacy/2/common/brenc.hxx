/* Copyright 2020 Roger Merryfield
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 * proj: Braille8 Display Reference Prototype
 * file: brenc.hxx
 * date: 2020-12-12
 * desc: Prototypes of transcoding subroutines.
 *
 * */

#include "brdefs.hxx"

/* utf_clen
 * params:
 *   utfchar8_t c - the first byte of a UTF-8 code point
 * return:
 *   int          - the length of the UTF-8 encoding of a code point
 *                  beginning with c
 * */
int utf_clen(utfchar8_t c);

/* utf_8_to_32
 * params:
 *   utfchar32_t *pout - code point output address
 *   utfchar8_t    *pc - char array input address
 * return:
 *   int               - length of the decoded code point
 * */
int utf_8_to_32(utfchar32_t *pout, utfchar8_t const *pc);

/* utf_8_to_32
 * params:
 *   utfchar32_t *pout - char array output address (must be six bytes)
 *   utfchar8_t     u  - code point
 * return:
 *   int               - length of the encoded code point
 * */
int utf_32_to_8(utfchar8_t *pout, utfchar32_t u);

#endif

