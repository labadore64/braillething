# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 0.5 of the common.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

import sys, gc, subprocess, ordinal
from itertools import chain

def to_hex_entities(s):
    return ''.join(f'&#{hex(ord(c))[1:]};' for c in s)

def repr_hex_ord(c):
    return f"0x{hex(ord(c))[2:]}"

def repr_hex_byte(byte):
    return f"0x{hex(byte)[2:]:>02}"

visible = frozenset(ord(x) for x in '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~')

def _repr_ucode(x, exclude=None):
    if exclude is not None:
        try:
            exc = chr(x) in exclude
            ischr = True
        except ValueError:
            exc = False
            ischr = False
        if exc or x in exclude:
            return exclude[chr(x) if ischr else x]

    return chr(x) if x in visible else f'u+{hex(x)[2:]}'

def repr_symbol_bytes(val):
    if isinstance(val, int):
        val = ordinal.OrdinalSet((val,))

    if isinstance(val, bytes):
        if len(val) == 1 and ord(val) in visible:
            val = val.decode('utf-8')
        else:
            val = ' '.join(repr_hex_byte(b) for b in val)
    elif isinstance(val, ordinal.OrdinalSet):
        result = []
        excl = {
            ',':  '[comma]',   '.':  '[period]',
            '(':  '[lparen]',  ')':  '[rparen]',
            '{':  '[lcurly]',  '}':  '[rcurly]',
            '[':  '[lbrace]',  ']':  '[rbrace]',
            ' ':  '[space]',   '\t': '[tabstop]',
            '\n': '[newline]', '\r': '[return]',
            '\v': '[tabvert]'
            } 
        for r in val.ranges:
            if len(r) == 1:
                result.append(_repr_ucode(r[0], exclude=excl))
            elif len(r) == 2:
                result.append(
                    f'{_repr_ucode(r[0], excl)},{_repr_ucode(r[-1], excl)}')
            elif len(r) > 2:
                result.append(
                    f'[{_repr_ucode(r[0], excl)}, {_repr_ucode(r[-1], excl)}]')
        val = ','.join(result)
        result.clear()
    return val

def set_of(*args):
    return frozenset(args)

def union_of(*args):
    return frozenset(chain(*args))

def intersection_of(*args):
    if len(args) == 0:
        return frozenset()

    first, *rest = args
    if rest:
        return frozenset(first) & intersection_of(*rest)
    else:
        return first

def dump_table(tab, padding=2, file=None):
    maxcolwidths = [len(max((str(y) for y in x), key=len)) + padding for x in zip(*tab)]
    if file is None:
        file = sys.stdout
    for row in tab:
        print(
            ' '.join(f'{str(cell):<{width}}' for (width, cell) in zip(maxcolwidths, row)),
            file=file)

def merge_pdfs(outfile, pdfs, what=None, verbose=False):
    command = list(chain(
        ('gs', '-dNOPAUSE', '-sDEVICE=pdfwrite', '-dBATCH'),
        (f'-sOUTPUTFILE={outfile}',),
        pdfs))

    if verbose:
        if what is None:
            ext = ''
        else:
            ext = f'for {what} '

    try:
        subprocess.run(command,
            check=True, stdout=subprocess.DEVNULL)
        if verbose:
            print(f"Collated PDF results {ext}are in {outfile}.")
        return True
    except Exception as e:
        if verbose:
            print(f"Collating PDF results {ext}failed!", e)
            print(f"  Command was {' '.join(command)}")
        return False

def render_dot_pdf(filename, what=None, verbose=False):
    if verbose and what is None:
        what = f'PDF for {filename}.dot'
    try:
        subprocess.run(['dot', '-Tpdf', f'{filename}.dot', f'-o{filename}.pdf'], check=True)
        if verbose:
            print(f" The rendering of the {what} is in {filename}.pdf.")
        return True
    except Exception:
        if verbose:
            print(f" The rendering of the {what} was unsuccessful because:")
            traceback.print_exc()
            return False
        else:
            raise

class file_tee:
    __slots__ = ['_fds']

    def __init__(self, *fds):
        self._fds = tuple(fds)

    def __del__(self) :
        for fd in self._fds:
            if fd != sys.stdout and fd != sys.stderr:
                fd.close()

    def write(self, data):
        for fd in self._fds:
            fd.write(data)

    def flush(self):
        for fd in self._fds:
            fd.flush()

def _collect(gen=2):
    return gc.collect(gen)

class _gc_halt:
    __slots__ = ['_B']

    def __init__(self):
        self._B = []
    def __enter__(self):
        self._B.append(gc.isenabled())
        if self._B[-1]:
            gc.disable()
        return _collect
    def __exit__(self, type, value, traceback):
        if self._B.pop():
            gc.enable()

gc_halt = _gc_halt()

