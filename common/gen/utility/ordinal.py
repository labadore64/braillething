# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 0.2 of the ordinal.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

# This is an implementation of a replacement for set that is designed to be
# optimal when storing massive sets of mostly contiguous numbers. In
# particular, it is very useful for representing regular language alphabets
# with massive quantities of symbols, such as the entirety of unicode.

from collections import abc
from itertools import chain, cycle, count

def _binary_search(X, value, cmp):
    L, U, i = 0, len(X) - 1, 0

    while L <= U:
        i = (L + U) >> 1
        u = cmp(X[i], value)

        if u == 0:
            break
        elif u < 0:
            L = i + 1
        elif u > 0:
            U = i - 1

    return i

def _cmp_range_ord(r, o):
    if r[0] == o:
        return 0
    elif r[0] > o:
        return 1
    elif r[0] < o:
        return -1

def _locate_ord(ranges, x):
    return _binary_search(ranges, x, _cmp_range_ord)

def _merge_ranges(r, s):
    if r[0] > s[0]:
        return _merge_ranges(s, r)

    # assuming r[0] <= s[0]
    if r[-1] + 1 >= s[0]:
        if r[-1] > s[-1]:
            return r
        else:
            return range(r[0], s[-1] + 1)
    else:
        return None

def _insert_range(ranges, r):
    if len(ranges) == 0:
        ranges.append(r)
    elif r[-1] < ranges[0][0] - 1:
        ranges.insert(0, r)
    elif r[0] > ranges[-1][-1] + 1:
        ranges.append(r)
    else:
        i = _locate_ord(ranges, r[0])
        while i < len(ranges) and ranges[i][-1] < r[0] - 1:
            i += 1
        if i > 0 and ranges[i - 1][-1] >= r[0] - 1:
            i -= 1
        for j, s in zip(count(i), ranges[i:]):
            t = _merge_ranges(r, s)
            if t is None:
                if j > i:
                    ranges[i:j] = (r,)
                else:
                    ranges.insert(i, r)
                return ranges
            else:
                r = t
        ranges[i:] = (r,)
    return ranges

def _ranges_contains(ranges, x):
    if len(ranges) == 0:
        return False, None
    elif x < ranges[0][0]:
        return False, 0
    elif x > ranges[-1][-1]:
        return False, len(ranges) - 1
    else:
        i = _locate_ord(ranges, x)
        if i > 0 and ranges[i - 1][-1] >= x:
            i -= 1
        if x in ranges[i]:
            return True, i
        else:
            return False, i

def _ranges_remove(ranges, r):
    if len(ranges) == 0:
        return ranges

    _, i = _ranges_contains(ranges, r[0])
    _, j = _ranges_contains(ranges, r[-1])

    chunk = tuple(_ranges_diff(ranges[i:j+1], (r,)))
    ranges[i:j+1] = chunk

def _ranges_zipper_tail(L, R, v, it):
    yield 'STOP', L
    if v is not None: yield R, v
    yield from zip(cycle(R), it)
    yield 'STOP', R

def _ranges_zipper(A, B):
    ia, ib = iter(A), iter(B)
    a, b = None, None
    try:
        side = 'A'; a = next(ia)
        side = 'B'; b = next(ib)
        while 1:
            if a[0] <= b[0]:
                yield 'A', a
                side = 'A'; a = next(ia)
            else:
                yield 'B', b
                side = 'B'; b = next(ib)
    except StopIteration:
        if side == 'A':
            yield from _ranges_zipper_tail('A', 'B', b, ib)
        else:
            yield from _ranges_zipper_tail('B', 'A', a, ia)

def _ranges_union(A, B):
    it = iter(_ranges_zipper(A, B))

    side, result = next(it)
    if side == 'STOP':
        for side, r in it:
            if side != 'STOP':
                yield r
    else:
        for side, r in it:
            if side != 'STOP':
                s = _merge_ranges(r, result)
                if s is None:
                    yield result
                    result = r
                else:
                    result = s

        if result is not None:
            yield result

def _ranges_diff(A, B):
    it = iter(_ranges_zipper(A, B))
    result = []
    stk = []
    last_B = None
    stop_A = False
    stop_B = False
    for side, r in it:
        if side == 'A':
            out = None
            if last_B is None or last_B[-1] < r[0]:
                out = r
            elif last_B[-1] < r[-1]:
                out = range(last_B[-1] + 1, r[-1] + 1)
            if out is not None:
                if stop_B:
                    if result:
                        yield from result
                        result.clear()
                    yield out
                else:
                    result.append(out)
        elif side == 'B':
            last_B = r
            if result:
                while result and r[0] <= result[-1][-1]:
                    if r[-1] >= result[-1][0]:
                        if r[-1] < result[-1][-1]:
                            stk.append(range(r[-1] + 1, result[-1][-1] + 1))
                        if r[0] > result[-1][0]:
                            stk.append(range(result[-1][0], r[0]))
                    else:
                        stk.append(result[-1])
                    result.pop() # pop off

                yield from result
                result.clear()
                
                while stk: result.append(stk.pop())
                if stop_A and not result:
                    return
        else:
            # stop
            if r == 'A':
                stop_A = True
            else:
                stop_B = True

    yield from result
    result.clear()

def _ranges_symdiff(A, B):
    return _ranges_union(_ranges_diff(A, B), _ranges_diff(B, A))

def _ranges_isect(A, B):
    return _ranges_diff(_ranges_union(A, B), _ranges_symdiff(A, B))

def _to_ord_ranges(it, ranges = None):
    if not isinstance(it, abc.Iterable):
        raise TypeError('_to_ord_ranges: argument must be iterable')

    if ranges is None:
        ranges = []

    for x in it:
        if isinstance(x, range):
            _insert_range(ranges, x)
        elif isinstance(x, bytes):
            for b in x:
                _insert_range(ranges, range(b, b + 1))
        elif isinstance(x, str):
            for c in x:
                b = ord(c)
                _insert_range(ranges, range(b, b + 1))
        elif isinstance(x, int):
            _insert_range(ranges, range(x, x + 1))
        else:
            raise TypeError(f"_to_ord_ranges: \
illegal value in input iterable {repr(x)}")
    return ranges

def _ranges_extract(A):
    if isinstance(A, OrdinalSet):
        sranges = A._ranges
        slen = A._len
    else:
        sranges = _to_ord_ranges(A)
        slen = sum(len(r) for r in sranges)

    return sranges, slen

def _ranges_subset(A, B):
    sranges, slen = _ranges_extract(A)
    if slen == 0:
        return True # vacuously true

    oranges, olen = _ranges_extract(B)
    if slen > olen:
        return False # easy out

    # self is a subset of obj iff the intersection of self and obj
    # is equal to self
    n = 0
    for i, r in enumerate(_ranges_isect(sranges, oranges)):
        if i >= len(sranges) or sranges[i] != r:
            return False # range not equal or missing, can't be a subset
        n += 1

    if n != len(sranges):
        return False # not the right number of ranges, can't be a subset

    return True

# dense ordinal set
class OrdinalSet:
    __slots__ = ['_ranges', '_len', '_hash']

    def __init__(self, obj=None, _cons=False, _mutable=False):
        if _cons is tuple:
            self._ranges, self._len, self._hash = obj
        elif _cons:
            result = []
            rlen = 0
            for r in obj:
                rlen += len(r)
                result.append(r)
            
            if _mutable:
                self._ranges = result
            else:
                self._ranges = tuple(result)
                result.clear()

            self._len = rlen
            self._hash = None if _mutable else hash(self._ranges)

        elif obj is None:
            self._ranges = tuple()
            self._len = 0
            self._hash = hash(self._ranges)
        elif isinstance(obj, OrdinalSet):
            self._ranges = obj._ranges
            self._len = obj._len
            self._hash = obj._hash
            if isinstance(obj, MutableOrdinalSet):
                self._ranges = tuple(self._ranges)
                self._hash = hash(self._ranges)
        elif isinstance(obj, abc.Iterable):
            self._ranges = tuple(_to_ord_ranges(obj))
            self._len = sum(len(r) for r in self._ranges)
            self._hash = hash(self._ranges)
        else:
            raise TypeError("OrdinalSet.__init__: \
argument must be an instance of NoneType, OrdinalSet, or abc.Iterable")

    def __repr__(self):
        content = ', '.join(repr(x) for x in self._ranges)
        return f"{self.__class__.__name__}([{content}])"

    def __str__(self):
        visible = frozenset(ord(x) for x in '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~')
        _repr_ucode = lambda x: chr(x) if x in visible else f'u+{hex(x)[2:]}'

        strrr = lambda r: f"[{_repr_ucode(r[0])}, {_repr_ucode(r[-1])}]"
        strr = lambda r: strrr(r) if len(r) > 1 else (_repr_ucode(r[0]) if r[0] >= 0 else str(r[0]))
        return '\u222a'.join(strr(x) for x in self._ranges)

    def __len__(self):
        return self._len

    def __hash__(self):
        if self._hash is not None:
            return self._hash
        else:
            raise TypeError(f"{self.__class__.__name__} is unhashable!")

    def __bool__(self):
        return len(self._ranges) != 0

    @property
    def lower_bound(self):
        if self._ranges:
            return self._ranges[0][0]
        else:
            return None

    @property
    def upper_bound(self):
        if self._ranges:
            return self._ranges[-1][-1]
        else:
            return None

    @property
    def ranges(self):
        if self._hash is None:
            # it's mutable, don't allow modification
            return tuple(self._ranges)
        else:
            return self._ranges

    @property
    def ordinals(self):
        for r in self._ranges:
            for x in r:
                yield x

    @property
    def codepoints(self):
        return (chr(x) for x in self.ordinals)

    def __iter__(self):
        return iter(self.ordinals)

    def __contains__(self, x):
        ok, i = _ranges_contains(self._ranges, x)
        return ok

    def __ne__(self, obj):
        if isinstance(obj, OrdinalSet):
            if self._len != obj._len or self._hash != obj._hash:
                return True
            else:
                return self._ranges != obj._ranges
        else:
            return True

    def __eq__(self, obj):
        return not self.__ne__(obj)

    def isdisjoint(self, obj):
        if isinstance(obj, OrdinalSet):
            for _ in _ranges_isect(self._ranges, obj._ranges):
                return False
            return True
        elif isinstance(obj, abc.Iterable):
            for _ in _ranges_isect(self._ranges, _to_ord_ranges(obj)):
                return False
            return True
        else:
            raise TypeError('OrdinalSet.isdisjoint: \
argument must be OrdinalSet or an iterable.')

    def issubset(self, obj):
        if isinstance(obj, abc.Iterable):
            return _ranges_subset(self, obj)
        else:
            raise TypeError('OrdinalSet.issubset: \
argument must be OrdinalSet or an iterable.')

    def issuperset(self, obj):
        if isinstance(obj, abc.Iterable):
            return _ranges_subset(obj, self)
        else:
            raise TypeError('OrdinalSet.issuperset: \
argument must be OrdinalSet or an iterable.')

    def __le__(self, obj):
        # subset relation
        return self.issubset(obj)

    def __lt__(self, obj):
        # proper subset relation
        return self.__ne__(obj) and self.issubset(obj)

    def __ge__(self, obj):
        # superset relation
        return self.issuperset(obj)

    def __gt__(self, obj):
        # proper superset relation
        return self.__ne__(obj) and self.issuperset(obj)

    def union(self, *objs):
        if len(objs) == 0:
            return self

        sranges = self._ranges
        for oranges, _ in (_ranges_extract(obj) for obj in objs):
            sranges = _ranges_union(sranges, oranges)

        return OrdinalSet(sranges, _cons=True, _mutable=self._hash is None)

    def intersection(self, *objs):
        if len(objs) == 0:
            return self

        sranges = self._ranges
        for oranges, _ in (_ranges_extract(obj) for obj in objs):
            sranges = _ranges_isect(sranges, oranges)

        return OrdinalSet(sranges, _cons=True, _mutable=self._hash is None)

    def difference(self, *objs):
        if len(objs) == 0:
            return self

        sranges = self._ranges
        for oranges, _ in (_ranges_extract(obj) for obj in objs):
            sranges = _ranges_diff(sranges, oranges)

        return OrdinalSet(sranges, _cons=True, _mutable=self._hash is None)

    def symmetric_difference(self, obj):
        oranges, _ = _ranges_extract(obj)
        return OrdinalSet(
            _ranges_symdiff(self._ranges, oranges),
            _cons=True,
            _mutable=self._hash is None)

    def __or__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('OrdinalSet.__or__: \
argument must be an instance of OrdinalSet')

        return self.union(other)

    def __and__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('OrdinalSet.__and__: \
argument must be an instance of OrdinalSet')

        return self.intersection(other)

    def __sub__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('OrdinalSet.__sub__: \
argument must be an instance of OrdinalSet')

        return self.difference(other)

    def __xor__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('OrdinalSet.__xor__: \
argument must be an instance of OrdinalSet')

        return self.symmetric_difference(other)

# dense ordinal set
class MutableOrdinalSet(OrdinalSet):
    __slots__ = []

    def __init__(self, obj=None, _cons=False):
        if _cons or _cons is tuple:
            OrdinalSet.__init__(self, obj, _cons=_cons, _mutable=True)
        elif obj is None:
            OrdinalSet.__init__(self,
                ([], 0, None),
                _cons=tuple)
        elif isinstance(obj, OrdinalSet):
            OrdinalSet.__init__(self,
                (list(obj._ranges), obj._len, None),
                _cons=tuple)
        elif isinstance(obj, abc.Iterable):
            ranges = _to_ord_ranges(obj)
            OrdinalSet.__init__(self,
                (ranges, sum(len(r) for r in ranges), None),
                _cons=tuple) 
        else:
            raise TypeError("MutableOrdinalSet.__init__: \
argument must be an instance of NoneType, OrdinalSet, or abc.Iterable")

    def update(self, *objs):
        if len(objs) > 0:
            sranges = tuple(self._ranges)
            for oranges, _ in (_ranges_extract(obj) for obj in objs):
                sranges = _ranges_union(sranges, oranges)

            self._ranges = list(sranges)
            self._len = sum(len(r) for r in self._ranges)

    def intersection_update(self, *objs):
        if len(objs) > 0:
            sranges = tuple(self._ranges)
            for oranges, _ in (_ranges_extract(obj) for obj in objs):
                sranges = _ranges_isect(sranges, oranges)

            self._ranges = list(sranges)
            self._len = sum(len(r) for r in self._ranges)

    def difference_update(self, *objs):
        if len(objs) > 0:
            sranges = tuple(self._ranges)
            for oranges, _ in (_ranges_extract(obj) for obj in objs):
                sranges = _ranges_diff(sranges, oranges)

            self._ranges = list(sranges)
            self._len = sum(len(r) for r in self._ranges)

    def symmetric_difference_update(self, obj):
        oranges, _ = _ranges_extract(obj)
        self._ranges = list(_ranges_symdiff(tuple(self._ranges), oranges))
        self._len = sum(len(r) for r in self._ranges)

    def __ior__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('MutableOrdinalSet.__ior__: \
argument must be an instance of OrdinalSet')

        self.update(other)

    def __iand__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('MutableOrdinalSet.__iand__: \
argument must be an instance of OrdinalSet')

        self.intersection_update(other)

    def __isub__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('MutableOrdinalSet.__isub__: \
argument must be an instance of OrdinalSet')

        self.difference_update(other)

    def __ixor__(self, other):
        if not isinstance(other, OrdinalSet):
            raise ValueError('MutableOrdinalSet.__ixor__: \
argument must be an instance of OrdinalSet')

        self.symmetric_difference_update(other)

    def multiadd(self, elem):
        if isinstance(elem, (int, str, bytes, range)):
            _to_ord_ranges((elem,), ranges=self._ranges)
            self._len = sum(len(r) for r in self._ranges)
        else:
            raise TypeError("MutableOrdinalSet.multiadd: \
cannot add element that is not an int, a str, a bytes, or a range.")

    def add(self, elem):
        onesym = isinstance(elem, (str, bytes, range)) and len(elem) == 1
        if onesym or isinstance(elem, int):
            self.multiadd(elem)
        else:
            raise TypeError("MutableOrdinalSet.add: \
cannot add element that is not an int, a str of length 1, \
a bytes of length 1, or a range of length 1.")

    def remove(self, elem):
        onesym = isinstance(elem, (str, bytes, range)) and len(elem) == 1
        if onesym or isinstance(elem, int):
            if isinstance(elem, (str, bytes)):
                elem = ord(elem)
            elif isinstance(elem, range):
                elem = elem[0]
            has, _ = _ranges_contains(self._ranges, elem)
            if not has:
                raise KeyError()

            _ranges_remove(self._ranges, range(elem, elem+1))
            self._len -= 1
        else:
            raise TypeError("MutableOrdinalSet.remove: \
cannot remove element that is not an int, a str of length 1, \
a bytes of length 1, or a range of length 1.")

    def multidiscard(self, elem):
        if isinstance(elem, (int, str, bytes, range)):
            for r in _to_ord_ranges((elem,)):
                _ranges_remove(self._ranges, r)
            self._len = sum(len(r) for r in self._ranges)
        else:
            raise TypeError(f'MutableOrdinalSet.multidiscard: {repr(elem)} \
cannot be discarded unless it is an int, a str, a bytes, or a range.')

    def discard(self, elem):
        onesym = isinstance(elem, (str, bytes, range)) and len(elem) == 1
        if onesym or isinstance(elem, int):
            self.multidiscard(elem)
        else:
            raise TypeError("MutableOrdinalSet.discard: \
cannot remove element that is not an int, a str of length 1, \
a bytes of length 1, or a range of length 1.")

    def pop(self):
        if not self._ranges:
            raise ValueError("MutableOrdinalSet.pop: nothing in set.")
        if len(self._ranges[-1]) == 1:
            value = self._ranges[-1][0]
            self._ranges.pop()
        else:
            value = self._ranges[-1][-1]
            self._ranges[-1] = range(self._ranges[-1][0], value)
        self._len -= 1
        return value

    def clear(self):
        self._ranges.clear()
        self._len = 0
