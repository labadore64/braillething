# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 0.13 of the regular.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

from collections import namedtuple, deque
import common, automata, ordinal

# a regular expression compiler which outputs a minimal DFA from an input
# regular expression. accepts almost canonical regular expressions with
# notational extensions for fixed range repetitions and possible
# concatenations, not regex.

ReOp = namedtuple('ReOp', ['prec', 'assoc', 'arity'])

# RE OPERATORS
re_ops = {
    'CAT': ReOp(prec=4, assoc='L', arity=2),    # concatenation
    '*':   ReOp(prec=3, assoc='Post', arity=1), # repetition
    '+':   ReOp(prec=3, assoc='Post', arity=1), # positive repetition
    '?':   ReOp(prec=3, assoc='Post', arity=1), # conditional
    '^':   ReOp(prec=4, assoc='R', arity=2),    # power concatination
    '|':   ReOp(prec=2, assoc='L', arity=2),    # alternation
    '(':   ReOp(prec=9, assoc='L', arity=0),    # grouping
    ')':   ReOp(prec=0, assoc='L', arity=0)
    }

# RE OPERATOR NAMES
re_opnames = {
    'CAT': ('CAT', 'Concatenation'),
    '*':   ('REP', 'Repetition'),
    '+':   ('RPP', 'Positive Repetition'),
    '?':   ('CND', 'Conditional'),
    '^':   ('POW', 'PowerCat'),
    '|':   ('ALT', 'Alternation')
    }

# RE LEXICAL ANALYSIS
def _lex_re(s):
    accum = []
    mode = 'reg'
    ctr = 0
    first = None
    last = None
    allowneg = False
    emitalt = False
    isneg = False
    escape = False
    alphabet = ordinal.MutableOrdinalSet()
    for index, x in enumerate(s):
        if mode != 'num':
            yield 'IDX', str(index) # we will emit these as markers of position in
                                    # the source string

        if mode != 'num' and not escape and x == '\\':
            escape = True
            continue

        if mode == 'reg':
            if escape:
                yield 'CODE', ord(x)
                alphabet.add(x)
            elif x in re_ops:
                yield x, re_ops[x]
            elif x == '[':
                mode = 'sym'
                first = None
                allowneg = True
                isneg = False
                emitalt = False
                ctr = 2
                yield 'SYMCLASS', None
            elif x == '{':
                mode = 'num'
                first = None
                ctr = 2
                while accum: accum.pop()
            else:
                yield 'CODE', ord(x)
                alphabet.add(x)
        elif mode == 'sym':
            if escape:
                if first is None:
                    yield 'CODE', ord(x)
                    alphabet.add(x)
                    last = ord(x) + 1
                else:
                    last = ord(x) + 1
                    yield 'CODE', range(first, last)
                    alphabet.multiadd(range(first, last))
                    first = None
                    ctr += 1
            elif x == ']':
                mode = 'reg'
                yield 'ENDSYMCLASS', None
            elif x == '^' and allowneg:
                isneg = True
                yield 'NEG', None
            elif x == '-' and ctr <= 0:
                first, ctr = last, 2
            elif first is not None:
                last = ord(x) + 1
                yield 'CODE', range(first, last)
                alphabet.multiadd(range(first, last))
                first = None
            else:
                yield 'CODE', ord(x)
                alphabet.add(x)
                last = ord(x) + 1
            allowneg = False
            
        elif mode == 'num':
            if x == '}':
                mode = 'reg'
                if first is None:
                    first = int(''.join(accum))
                    yield 'REP', '*' + str(first)
                else:
                    last = int(''.join(accum))
                    yield 'REP', '*' + str(first) + '?' + str(last)
            elif x == '-' and ctr <= 0 and first is None:
                first = int(''.join(accum))
                while accum: accum.pop()
            else:
                accum.append(x)

        escape = False
        ctr -= 1

    yield 'IDX', str(len(s))
    yield 'ALPHA', alphabet

def L0re(s, alphabet=None):
    tokens = deque(_lex_re(s))

    accum = ordinal.MutableOrdinalSet()
    _, implicit_alphabet = tokens.pop()
    if alphabet is None:
        alphabet = ordinal.OrdinalSet(implicit_alphabet)
    else:
        alphabet = ordinal.OrdinalSet(implicit_alphabet).union(alphabet)
    
    last_index = None
    while tokens:
        tok, obj = tokens.popleft()
        if tok == 'IDX':
            last_index = obj
            yield tok, obj
        elif tok == 'SYMCLASS':
            # handle negations of symbol class alternations
            negate = False
            while 1:
                tok, obj = tokens.popleft()
                if tok == 'NEG':
                    negate = True
                elif tok == 'CODE':
                    accum.multiadd(obj)
                elif tok == 'ENDSYMCLASS':
                    break
            if negate:
                symclass = alphabet - accum
            else:
                symclass = ordinal.OrdinalSet(accum)
            accum.clear()    
            
            if len(symclass) == 0:
                raise ValueError(
                    f"Empty symbol class at index {last_index} is not allowed!")
            elif len(symclass) == 1:
                for code in symclass:
                    yield 'CODE', code
            else:
                # above the smallness threshold we
                yield 'CODE', symclass

        else:
            yield tok, obj


# RE IMPLICIT OP INSERTION
def L1re(g):
    last_tok, last_val = None, None
    for token, val in g:
        if token in ('CODE', 'REP', '('):
            if last_tok is not None and \
                    (last_tok not in re_ops or last_tok == ')' or \
                    (last_val.arity != 2 and \
                    (last_val.arity == 1 and last_val.assoc == 'Post'))):
                yield 'CAT', re_ops['CAT']
        
        yield token, val
        if token != 'IDX':
            last_tok, last_val = token, val

def table_append(tab, index, val, action, Q, stack, note):
    Q = ' '.join(common.repr_symbol_bytes(s) for _, s in Q)
    S = ' '.join(common.repr_symbol_bytes(s) for _, s, _ in stack)
    tab.append((index, common.repr_symbol_bytes(val), action, Q, S, note))

# RE SHUNTING YARD
def L2re(g):
    Q, stack = [], []
    tab = [('INDEX', 'TOKEN', 'ACTION', 'RPN OUTPUT', 'OP STACK', 'NOTES')]
    index = -1
    for token, val in g:
        if token == 'IDX':
            index = val
            continue

        note, action = '', ''
        if token == 'CODE':
            action = 'Add symbol class to output'
            Q.append((index, val))
            table_append(tab, index, val, action, Q, stack, note)
        elif token == 'REP':
            action = 'Add repetition specifier to output'
            Q.append((index, val))
            table_append(tab, index, val, action, Q, stack, note)
        elif token in re_ops:
            t1, (p1, a1, n1) = token, val
            v = t1
            if n1 == 1:
                if a1 == 'Pre': # prefix 
                    action = 'Push prefix unary op token to stack'
                    stack.append((index, token, val))
                    table_append(tab, index, v, action, Q, stack, note)
                else:
                    action = 'Add postfix unary op token to output'
                    Q.append((index, v))
                    table_append(tab, index, v, action, Q, stack, note)
            else:
                note = 'Pop ops from stack to output'
                while stack:
                    i2, t2, (p2, a2, n2) = stack[-1]
                    if (a1 == 'L' and p1 <= p2) or (a1 == 'R' and p1 < p2):
                        if t1 != ')':
                            if t2 != '(':
                                stack.pop()
                                action = '(Pop op)'
                                Q.append((i2, t2))
                            else:
                                break
                        else:
                            if t2 != '(':
                                stack.pop()
                                action = '(Pop op)'
                                Q.append((i2, t2))
                            else:
                                stack.pop()
                                action = '(Pop & discard "(")'
                                table_append(tab, i2, v, action, Q, stack, note)
                                break
                        table_append(tab, i2, v, action, Q, stack, note)
                        v, note = '', ''
                    else:
                        note = ''
                        break
                    note = ''
                note = ''
                if t1 != ')':
                    stack.append((index, token, val))
                    action = 'Push op token to stack'
                else:
                    action = 'Discard ")"'
                table_append(tab, index, v, action, Q, stack, note)

    note = 'Drain stack to output'
    while stack:
        v = ''
        i2, t2, (p2, a2, n2) = stack[-1]
        action = '(Pop op)'
        stack.pop()
        Q.append((i2, t2))
        table_append(tab, i2, v, action, Q, stack, note)
        v, note = '', ''

    return tab, Q

def dump_L2re_table(rp, Q):
    common.dump_table(rp)
    print(f"\n The RPN is: {' '.join(str(i)+':'+common.repr_symbol_bytes(q) for i, q in Q)}")

# RE AST CONSTRUCTION
def L3re(Q):
    stack = []
    for i, x in Q:
        if x in re_ops:
            _, _, n = re_ops[x]
            stack[-n:] = ((int(i), x, *stack[-n:]),)
        else:
            stack.append((int(i), x))

    return stack

# RE AST EXPANSION
def L4re(T):
    if not isinstance(T, tuple):
        return T # terminal

    index, op, *args = T
    if op == '^': # expand fixed repetition
        cat = None
        subT = L4re(args[-2])
        index, op, *subargs = subT
        if '?' in args[-1][-1]:
            # it's a range repetition (N to M), nest conditional M - N times, nest N times
            N, M = (int(u) for u in args[-1][-1][1:].split('?', 1))
            for i in range(N):
                cat = (index, 'CAT', cat, subT) if cat is not None else subT
            for i in range(M - N):
                cat = (index, 'CAT', cat, (index, '?', subT)) if cat is not None else (index, '?', subT)
            if cat is None:
                raise ValueError('fixed repetition must at least specify one concatination')
        else:
            # it's a fixed repetition (N), nest N times
            N = int(args[-1][-1][1:])
            for i in range(N):
                cat = (index, 'CAT', cat, subT) if cat is not None else subT
            if cat is None:
                raise ValueError('fixed repetition must at least specify one concatination')
        return cat
    else:
        return (index, op, *(L4re(U) for U in args))

def re_to_AST(re, alphabet=None, table=False):
    rp, Q = L2re(L1re(L0re(re, alphabet=alphabet)))

    result = L3re(Q)
    if len(result) != 1:
        raise RuntimeError(f"Excess stack elements after AST construction \
indicating malformed RPN: {result}")

    expanded = L4re(*result)
    if table:
        return expanded, rp
    else:
        return expanded

def dump_AST(ast, f, title=None):
    # perform a breadth first traversal of the AST to correctly order the nodes
    # by rows using the deque method. emit the nodes in breadth first order,
    # then emit the edges in the same order the nodes were visited, and
    # finally, tell graphvis to group each row at the same rank so that the
    # layout stays nice and clean.
    print("digraph G {", file=f)
    if title is not None:
        print(f"  graph [label=<{title}>]", file=f)
    print("  node [shape=record];", file=f)
    print("  ranksep=0.8;", file=f)

    ctr = 0
    Q = deque()
    Q.append((0, 0, ast))
    levels = {}
    edges = []

    # quick function for making a hex html entity
    entity = lambda c: f"&#{hex(ord(c))[1:]};"

    while Q:
        ident, level, node = Q.popleft()
        if level not in levels:
            levels[level] = []
        levels[level].append((ident, node))

        if node[1] in re_opnames:
            name = re_opnames[node[1]][0]
        else:
            name = ''.join(entity(c) for c in common.repr_symbol_bytes(node[1]))

        if node[1] in re_ops:
            # it's not a terminal
            children = node[2:]
            nch = len(children)
            if nch > re_ops[node[1]].arity:
                # it's a flattened associative op
                name = f"{name} ({nch})"

        print(f"  n{ident} [label=\"{node[0]}|{name}\"]", file=f)

        if node[1] in re_ops:
            # it's not a terminal
            Q.extend((ctr+i+1, level+1, ch) for i, ch in enumerate(children))
            edges.extend((ident, ctr+i+1) for i in range(nch))
            ctr += nch

    for source, sink in edges:
        print(f"  n{source} -> n{sink} [headport=n];", file=f)

    for level in sorted(levels.keys()):
        print("  { rank=same; ", end="", file=f)
        nodes = levels[level]
        for ident, _ in nodes:
            print(f"n{ident} ", end="", file=f);
        print("}", file=f)

    print("}", file=f)

# RE LEVEL 4 SYNTAX TREE TO NFA
def AST_to_NFA(T, incremental=True, encoding=None, endianness=None):
    index, op, *args = T
    subM = [AST_to_NFA(U, incremental, encoding, endianness) for U in args]
    if len(args) == 0:
        if encoding is None: 
            M = automata.NFA_sym(op)
        else:
            M = automata.NFA_sym_unicode(op, encoding, endianness)
    elif op == '*':
        M = automata.NFA_rep(*subM)
    elif op == '+':
        M = automata.NFA_repp(*subM)
    elif op == '?':
        M = automata.NFA_cond(*subM)
    elif op == 'CAT':
        M = automata.NFA_cat(*subM)
    elif op == '|':
        M = automata.NFA_alt(*subM)
    else:
        raise RuntimeError(f"bad op {repr(op)} produced from index {index}")
    
    if incremental:
        M = automata.NFA_to_DFA(M)
        labels = automata.new_state_labels(M)
        M = automata.automata_labeling(M, labels)
        P = automata.DFA_hopcroft_partition(M)
        M = automata.DFA_myhill_nerode_reduction(M, P)
        M = automata.DFA_to_NFA(M)

    return M

def re_to_NFA(re, alphabet=None, incremental=True, \
        encoding=None, endianness=None, table=False):

    ast = re_to_AST(re, alphabet=alphabet, table=table)
    M = AST_to_NFA(ast[0] if table else ast,
        incremental=incremental,
        encoding=encoding,
        endianness=endianness)

    if table:
        return M, ast[1]
    else:
        return M

def re_to_DFA(re, alphabet=None, encoding=None, endianness=None):
    return automata.NFA_to_DFA(
        re_to_NFA(re, alphabet=alphabet,
            encoding=encoding, endianness=endianness))

# get the implicit alphabet of an RE
def re_to_alphabet(re):
    for tok, obj in _lex_re(re):
        if tok == 'ALPHA':
            return ordinal.OrdinalSet(obj)
    return ordinal.OrdinalSet()

def res_to_DFPA(res, minimize=True, alphabet=None, \
        encoding=None, endianness=None):

    res = tuple(res)
    if alphabet is None:
        alphabet = ordinal.OrdinalSet()
    else:
        alphabet = ordinal.OrdinalSet(alphabet)

    alphabet = alphabet.union(*(re_to_alphabet(re) for re in res))

    Ms = []
    for re in res:
        Ms.append(re_to_DFA(re,
            alphabet=alphabet, encoding=encoding, endianness=endianness))
    M, L_map = automata.automata_list_to_DFPA(Ms, minimize=minimize)
    return M, L_map

