# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 0.1 of the fsm.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

import common, automata, alphabet
from collections import abc
from itertools import zip_longest, chain
from ordinal import OrdinalSet

class FSMSpecification:
    __slots__ = ['_M', '_table', '_P', '_cache']

    @classmethod
    def _check_partition(self, name, argname, M, P):
        if P is None:
            P = (M.Q,)
        elif not isinstance(P, abc.Iterable):
            raise ValueError(
                f"{name}: argument {argname} must be either None, or it "
                f"must be a partition of the states of M, but got {P}.")
        elif not isinstance(P, tuple):
            P = tuple(P)

        return P

    def _init_empty(self):
        self._M = None
        self._P = None

    def _init_machine(self, M, P):
        self._M = M
        self._P = P

    def __init__(self, M=None, P=None):
        self._table = []
        if M is None:
            self._init_empty()
        elif not isinstance(M, automata.DFA):
            raise ValueError(
                "FSMSpecification.__init__: "
                "argument M must be a DFA.")
        else:
            P = self._check_partition(
                self.__class__.__name__ + '.__init__',
                'P', M, P)

            self._init_machine(M, P)

    @property
    def state_partition(self):
        return self._P

    @state_partition.setter
    def state_partition(self, P):
        if self._M is None:
            raise ValueError(
                "FSMSpecification.state_partition.setter: "
                "cannot set partition when M is still None.")

        P = self._check_partition(
            self.__class__.__name__ + '.state_partition.setter',
            'P', P)

        if self._P != P:
            self._table.clear()
        self._P = P

    @property
    def automata(self):
        return self._M

    @automata.setter
    def automata(self, M):
        if not isinstance(M, automata.DFA):
            raise ValueError(
                "FSMSpecification.automata.setter: "
                "argument must be a DFA.")

        if self._M != M:
            self._table.clear()
            self._P = (M.Q,)
        self._M = M

    @property
    def alphabet(self):
        if self._M is not None:
            return self._M.alphabet
        else:
            return OrdinalSet()

    def clear(self):
        self._M = None
        self._table.clear()

    @property
    def table(self):
        return self.get_table(display=False)

    @property
    def display_table(self):
        return self.get_table(display=True)

    def get_table(self, display=False):
        if not self._table:
            if self._M is None:
                raise ValueError(
                    "FSMSpecification.table: cannot "
                    "generate a state machine when no automata is present.")

            mapping = dict()
            states = self._table

            for mark, *data in automata.automata_walk(self._M):
                if mark == 'STATE':
                    q, kind, level = data
                    mapping[q] = len(states)
                    states.append(([], kind == 'FINAL', []))
                elif mark == 'TRANS':
                    q, r, E = data
                    states[mapping[q]][0].append((mapping[r], E))
            
            finished_states = []
            for label, row in enumerate(states):
                transitions, isfinal, annotations = row
                transitions.sort(key=lambda pair: pair[0])
                if len(transitions) == 1:
                    target, E = transitions[0]
                    if label == target and E == self._M.alphabet:
                        annotations.append('ERROR')

            self._cache = tuple((tuple(t), f, tuple(a)) for t, f, a in self._table)

        if display:
            display_states = []
            display_states.append((
                'LABEL', 'INPUT', 'GOTO', 'FINAL?', 'ANNOTATIONS'))

            for i, row in enumerate(self._table):
                transitions, isfinal, annotations = row
                inputs = iter(E for label, E in transitions)
                gotos = iter(label for label, E in transitions)
                annos = iter(str(a) for a in annotations)
                firstinput = next(inputs)
                firstgoto = next(gotos)
                firstanno = next(annos) if annotations else ''
                display_states.append((
                    str(i),
                    firstinput,
                    firstgoto,
                    'YES' if isfinal else 'NO',
                    firstanno
                    ))
                for i, g, a in zip_longest(inputs, gotos, annos, fillvalue=''):
                    display_states.append(('', i, g, '', a))

            return display_states
        else:
            return self._cache

class FSM:
    __slots__ = ['_table', '_q', '_ab']

    def __init__(self, state_machine):
        if isinstance(state_machine, automata.NFA):
            M = automata.NFA_to_DFA(state_machine, minimize=True)
            state_machine = FSMSpecification(M)
        elif isinstance(state_machine, automata.DFA):
            state_machine = FSMSpecification(state_machine)
        elif not isinstance(state_machine, FSMSpecification):
            raise ValueError(
                "FSM.__init__: argument must be an instance of NFA, DFA, or "
                "FSMSpecification.")

        self._q = 0
        self._table = state_machine.get_table(display=False)
        self._ab = state_machine.alphabet

    @property
    def accepting(self):
        return self._table[self._q][1]

    @property
    def annotations(self):
        return self._table[self._q][2]

    @property
    def error(self):
        return 'ERROR' in self.annotations

    def reset(self):
        self._q = 0

    def next_state(self, a):
        x = ord(a)
        if x not in self._ab:
            raise ValueError(
                f"FSM.next_state: {a} is not in the alphabet {self._ab} of "
                "the FSM.")

        for q, E in self._table[self._q][0]:
            if x in E:
                return q

        raise ValueError(
            f"FSM.next_state: {a} is not in any transition set of the state "
            f"{self._q}, so the FSM must be malformed.")

    def input(self, a, table=None):
        if table is not None:
            table.append((self._q, '', 'INITIAL'))
            if self.accepting:
                table.append(('', '', 'YES'))
            for anno in self.annotations:
                table.append(('', '', anno))
        N = len(a)
        for c in a:
            last_q = self._q
            self._q = self.next_state(c)
            if table is not None:
                result = (f'FROM {last_q}',)
                if self.accepting:
                    result = result + ('YES',)
                result = result + self.annotations
                table.append((self._q, OrdinalSet(c), result[0]))
                for anno in result[1:]:
                    table.append(('', '', anno))
            N -= 1
            if self.error:
                break
        return self._q, self.annotations

    def run(self, s, tabulate=False):
        self.reset()
        if tabulate:
            table = [('LABEL', 'INPUT', 'ANNOTATIONS')]
        else:
            table = None

        self.input(s, table)

        if tabulate:
            anno = 'ACCEPT' if self.accepting else 'REJECT'
            table.append(('', '', anno))
            return table
        else:
            return self.accepting, self.annotations

