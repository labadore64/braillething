# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 0.3 of the machine.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

from collections import abc
from itertools import chain, cycle, count
import ordinal

# murmur3 hash
try:
    import mmh3 as mmh3
except ImportError:
    import pymmh3 as mmh3

class _epsilon_object:
    __slots__ = []
    def __repr__(self):
        return 'EPSILON'
    def __hash__(self):
        return 0
    def __eq__(self, other):
        return self is other
    def __ne__(self, other):
        return self is not other
    def __lt__(self, other):
        return False
    def __gt__(self, other):
        return self is not other
    def __le__(self, other):
        return self == other
    def __ge__(self, other):
        return True

EPSILON = _epsilon_object()
EPSILON_ORDINAL = -255
EPSILON_SET = ordinal.OrdinalSet((EPSILON_ORDINAL,))
_EPSILON_REPR = lambda e: EPSILON if EPSILON is e else e
_EPSILON_STR = lambda e: '\u03b5' if EPSILON is e else str(e)

class State:
    __slots__ = ['_q']
    _Ctr = 0
    def __init__(self):
        State._Ctr += 1
        self._q = State._Ctr
    def __repr__(self):
        return f'[q{self._q}]'
    def __hash__(self):
        return self._q
    def __eq__(self, other):
        return isinstance(other, State) and other._q == self._q
    def __ne__(self, other):
        return not isinstance(other, State) or other._q != self._q
    def __lt__(self, other):
        if isinstance(other, State):
            return self._q < other._q
        else:
            return False
    def __gt__(self, other):
        if isinstance(other, State):
            return self._q > other._q
        else:
            return True
    def __le__(self, other):
        return self == other or self < other
    def __ge__(self, other):
        return self == other or self > other

def encoding_endianness_to_py(encoding, endianness):
    encoding = encoding.lower()
    if encoding == 'unicode':
        return None
    elif encoding in ('asc', 'ascii'):
        return 'ascii'
    elif encoding in ('utf8', 'utf-8'):
        return 'utf-8'
    endianness = endianness.lower()
    if endianness in ('b', 'be', 'big'):
        if encoding in ('utf16', 'utf-16'):
            return 'utf-16-be'
        elif encoding in ('utf32', 'utf-32'):
            return 'utf-32-be'
    elif endianness in ('l', 'le', 'little'):
        if encoding in ('utf16', 'utf-16'):
            return 'utf-16-le'
        elif encoding in ('utf32', 'utf-32'):
            return 'utf-32-le'
    else:
        raise ValueError(f'encoding_endianness_to_py: bad endianness {endianness}')
    raise ValueError(f'encoding_endianness_to_py: bad encoding {encoding}')

_mmh3_seed = 0x9747b28c
_mmh3_empty_txset = mmh3.hash('TxSet()', 0x9747b28c)
class TxSet:
    __slots__ = ['_QRE', '_S', '_len', '_hash', '_keys']

    def __init__(self, obj=None):
        if obj is None:
            self._QRE = dict()
            self._S = ordinal.OrdinalSet()
            self._len = 0
            self._hash = _mmh3_empty_txset
            self._keys = tuple()
        elif isinstance(obj, TxSet):
            self._QRE = obj._QRE
            self._S = obj._S
            self._len = obj._len
            self._hash = obj._hash
            self._keys = obs._keys
        elif isinstance(obj, abc.Iterable):
            QRE = dict()
            S = ordinal.MutableOrdinalSet()
            
            for q, r, e in obj:
                if isinstance(e, ordinal.OrdinalSet):
                    if isinstance(e, ordinal.MutableOrdinalSet):
                        e = ordinal.OrdinalSet(e)
                elif isinstance(e, (str, bytes, range)):
                    e = ordinal.OrdinalSet((e,))
                elif e is EPSILON:
                    e = EPSILON_SET
                else:
                    raise ValueError(f"Bad transition label {e}.")

                S.update(e)
                if (q, r) in QRE:
                    QRE[(q, r)].update(e)
                else:
                    QRE[(q, r)] = ordinal.MutableOrdinalSet(e)

            n, H = 0, _mmh3_empty_txset
            self._keys = tuple(sorted(QRE.keys(), key=hash))
            for q, r in self._keys:
                e = ordinal.OrdinalSet(QRE[(q, r)])
                QRE[(q, r)] = e
                n += len(e)
                H = mmh3.hash('#'.join((repr((q, r, e)), str(n))), H)
            
            self._len = n
            self._QRE = QRE
            self._S = ordinal.OrdinalSet(S)
            self._hash = H
        else:
            raise ValueError

    def __len__(self):
        return self._len

    def __bool__(self):
        return self._len > 0
        
    def __repr__(self):
        if self._len == 0:
            return "TxSet()"
        
        content = (repr((q, r, _EPSILON_REPR(e))) for q, r, e in iter(self))
        return f"TxSet([{', '.join(content)}])"

    def __str__(self):
        if self._len == 0:
            return "Tx{}"
       
        content = []
        for q, r, e in iter(self):
            q, r = str(q), str(r)
            e = _EPSILON_STR(e)
            content.append(f"{q} - {e} {chr(0x2192)} {r}")

        return f"Tx{{{', '.join(content)}}}"
    
    def __eq__(self, other):
        if not isinstance(other, TxSet) or \
                self._len != other._len or \
                self._keys != other._keys:
            return False
        else:
            for q, r in self._keys:
                if self._QRE[(q, r)] != other._QRE[(q, r)]:
                    return False
            return True

    def __ne__(self, other):
        return not self.__eq__(other)

    @staticmethod
    def _emit_transition_set(q, r, e):
        if EPSILON_ORDINAL in e:
            yield q, r, EPSILON
            E = e - EPSILON_SET
            if E:
                yield q, r, E
        else:
            yield q, r, e

    def __contains__(self, pair):
        return pair in self._QRE

    def __getitem__(self, pair):
        if pair in self._QRE:
            e = self._QRE[pair]
            q, r = pair
            yield from self._emit_transition_set(q, r, e)

    def __iter__(self):
        for q, r in self._keys:
            yield from self._emit_transition_set(q, r, self._QRE[(q, r)])

    def _txmap(self, Q, E):
        QE = dict()
        for (q, r), e in self._QRE.items():
            if (Q is None or r in Q) and r not in QE:
                QE[r] = dict()
            if (Q is None or q in Q) and (E is None or not E.isdisjoint(e)):
                if q not in QE:
                    QE[q] = dict()
                QE[q][r] = e if E is None else e & E
        return QE

    # the reversed transition map associates sets of symbols to reverse
    # transitions rather than transitions to sets of symbols.
    def _txrmap(self, R, E):
        # TODO : use a pre-sorting algorithm to avoid full searches
        ER = []
        if E is None:
            E = self._S
        unused = ordinal.MutableOrdinalSet(self._S)
        inclusion = E - EPSILON_SET
        for (q, r), tx in self._QRE.items():
            if R is not None and r not in R:
                continue

            E = tx & inclusion
            unused.difference_update(E)
            while E:
                i = len(ER) - 1
                for e, _ in ER[::-1]:
                    if not e.isdisjoint(E):
                        break
                    i -= 1

                if i < 0:
                    ER.append((E, { r: (q,) }))
                    E = None
                else:
                    e, m = ER.pop(i)
                    shared = E & e
                    mnew = dict(m.items())
                    mnew[r] = (mnew[r] if r in mnew else ()) + (q,)

                    if e != shared:
                        ER.append((e - shared, m))
                    ER.append((shared, mnew))

                    E = E - shared
        ER.append((ordinal.OrdinalSet(unused), dict()))
        return ER

    @property
    def alphabet(self):
        return self._S

    def transition_map_of(self, Q, E=None, reversed=False):
        if not isinstance(Q, abc.Iterable):
            raise ValueError("TxSet.transition_map_of: Q must be an iterable.")
        return self._txrmap(Q, E) if reversed else self._txmap(Q, E)

    def transition_map(self, E=None, reversed=False):
        return self._txrmap(None, E) if reversed else self._txmap(None, E)

    def isdisjoint(self, other):
        if not isinstance(other, TxSet):
            other = TxSet(other)

        for (q, r), e in self._QRE.items():
            if (q, r) in other._QRE:
                if not e.isdisjoint(other._QRE[(q, r)]):
                    return False

        return True

    def _ssisect(self, other):
        if not isinstance(other, TxSet):
            other = TxSet(other)

        return other, self.intersection(other)

    def issubset(self, other):
        _, isct = self._ssisect(other)
        return self.__eq__(isct)

    def issuperset(self, other):
        other, isct = self._ssisect(other)
        return other.__eq__(isct)

    def __le__(self, other): # subset
        return self.issubset(other)

    def __ge__(self, other): # subset
        return self.issuperset(other)

    def __lt__(self, other): # subset
        return self.__ne__(other) and self.issubset(other)

    def __gt__(self, other): # subset
        return self.__ne__(other) and self.issuperset(other)

    def union(self, *others):
        if not others:
            return self

        first, *rest = others
        return TxSet(chain(self, first)).union(*rest)

    def _diff(self, other):
        if not isinstance(other, TxSet):
            other = TxSet(other)

        for (q, r), e in self._QRE.items():
            if (q, r) not in other._QRE:
                yield q, r, e
            else:
                yield q, r, e - other._QRE[(q, r)]

    def difference(self, *others):
        if not others:
            return self

        first, *rest = others
        return TxSet(self._diff(first.union(*rest)))

    def _isect(self, other):
        if not isinstance(other, TxSet):
            other = TxSet(other)

        for (q, r), e in self._QRE.items():
            if (q, r) in other._QRE:
                yield q, r, e & other._QRE[(q, r)]

    def intersection(self, *others):
        if not others:
            return self

        first, *rest = others
        return TxSet(self._isect(first)).intersection(*rest)
    
    def symmetric_difference(self, other):
        return self.union(other).difference(self.intersection(other))

    def __or__(self, other):
        if not isinstance(other, TxSet):
            raise ValueError('TxSet.__or__: \
argument must be an instance of TxSet')

        return self.union(other)

    def __and__(self, other):
        if not isinstance(other, TxSet):
            raise ValueError('TxSet.__and__: \
argument must be an instance of TxSet')

        return self.intersection(other)

    def __sub__(self, other):
        if not isinstance(other, TxSet):
            raise ValueError('TxSet.__sub__: \
argument must be an instance of TxSet')

        return self.difference(other)

    def __xor__(self, other):
        if not isinstance(other, TxSet):
            raise ValueError('TxSet.__xor__: \
argument must be an instance of TxSet')

        return self.symmetric_difference(other)

