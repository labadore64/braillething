# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 1.0 of the automata.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

from collections import deque, namedtuple, abc
from itertools import chain, count
from functools import lru_cache as memoized
from ordinal import OrdinalSet, MutableOrdinalSet
import sys, common, machine, heapq, alphabet

# epsilon transition object and state class
EPSILON = machine.EPSILON
State = machine.State

# NFA and DFA are identical in structure, except that they are nominally
# distinguished for purposes of visualization and validation
NFA = namedtuple('NFA', ['Q', 'trans', 'q0', 'F', 'alphabet'])
DFA = namedtuple('DFA', ['Q', 'trans', 'q0', 'F', 'alphabet'])

def NFA_bytestring(bs):
    q0 = State()
    Q = [q0]
    trans = []
    for b in bs:
        qN = State()
        trans.append((Q[-1], qN, OrdinalSet((b,))))
        Q.append(qN)
    return NFA(
        Q=frozenset(Q),
        trans=machine.TxSet(trans),
        q0=q0, F=common.set_of(Q[-1]),
        alphabet=OrdinalSet(bs))

# compose an alternation of bytestrings incrementally. returns None if the
# bytestrings iterable is empty and the optional NFA argument M is None.
def NFA_bytestring_alternation(bytestrings, M=None):
    if M is None:
        M = NFA_bytestring(bytestrings[0])
        foldlist = bytestrings[1:]
    else:
        foldlist = bytestrings
    for bs in foldlist:
        with common.gc_halt as collect:
            M = NFA_minimize(NFA_alt(M, NFA_bytestring(bs)))
            M = automata_labeling(M, new_state_labels(M))
    return M

def NFA_codes(codes, encoding, endianness):
    pyencoding = machine.encoding_endianness_to_py(encoding, endianness)
    ab = alphabet.code_alphabet_of(encoding)

    bytestrings = []
    M = None
    for x in codes.ordinals:
        if x not in ab:
            raise ValueError(
                f'NFA_codes: No ordinal {x} ({chr(x)}) in ' + \
                f'alphabet {ab} for encoding {encoding}.')
        
        if pyencoding is not None:
            bytestrings.append(chr(x).encode(pyencoding))
        else:
            bytestrings.append((x,))
        if len(bytestrings) >= 256:
            M = NFA_bytestring_alternation(bytestrings, M)
            bytestrings.clear()
    M = NFA_bytestring_alternation(bytestrings, M)
    bytestrings.clear()
    return M

def NFA_any_code_unicode():
    # Unicode any code machine
    q0, qF = State(), State()
    return NFA(
        Q=common.set_of(q0, qF),
        trans=machine.TxSet([(q0, qF, alphabet._unicode_alphabet)]),
        q0=q0, F=common.set_of(qF),
        alphabet=alphabet._unicode_alphabet)

def NFA_any_code_ascii():
    # ASCII any code machine
    q0, qF = State(), State()
    return NFA(
        Q=common.set_of(q0, qF),
        trans=machine.TxSet([(q0, qF, alphabet._ascii_alphabet)]),
        q0=q0, F=common.set_of(qF),
        alphabet=alphabet._ascii_alphabet)

_utf8_set_one_byte     = OrdinalSet([range(0x00, 0x80)])
_utf8_set_two_byte     = OrdinalSet([range(0xC0, 0xE0)])
_utf8_set_three_byte   = OrdinalSet([range(0xE0, 0xF0)])
_utf8_set_four_byte    = OrdinalSet([range(0xF0, 0xF8)])
_utf8_set_continuation = OrdinalSet([range(0x80, 0xC0)])
def NFA_any_code_utf8():
    # UTF-8 any code machine
    q0, qF = State(), State()

    q2b0 = State()                               # two byte path states
    q3b0, q3b1 = State(), State()                # three byte path states
    q4b0, q4b1, q4b2 = State(), State(), State() # four byte path states

    trans = machine.TxSet([
        # one byte encoding for range       0 -     7F
        (  q0,   qF, _utf8_set_one_byte),
        # two byte encoding for range      80 -    7FF
        (  q0, q2b0, _utf8_set_two_byte),
        (q2b0,   qF, _utf8_set_continuation),
        # three byte encoding for range   800 -   FFFF
        (  q0, q3b0, _utf8_set_three_byte),
        (q3b0, q3b1, _utf8_set_continuation),
        (q3b1,   qF, _utf8_set_continuation),
        # four byte encoding for range  10000 - 10FFFF
        (  q0, q4b0, _utf8_set_four_byte),
        (q4b0, q4b1, _utf8_set_continuation),
        (q4b1, q4b2, _utf8_set_continuation),
        (q4b2,   qF, _utf8_set_continuation)
        ])

    return NFA(
        Q=common.set_of(q0, qF, q2b0, q3b0, q3b1, q4b0, q4b1, q4b2),
        trans=trans,
        q0=q0, F=common.set_of(qF),
        alphabet=alphabet._utf8_alphabet)

_utf16_hi = OrdinalSet([range(0x00, 0xD8), range(0xE0, 0x100)])
_utf16_lo = alphabet._ascii_alphabet
_utf16_hi_surrogate_hi = OrdinalSet([range(0xD8, 0xDC)])
_utf16_lo_surrogate_hi = OrdinalSet([range(0xDC, 0xE0)])
_utf16_surrogate_lo = alphabet._ascii_alphabet
def NFA_any_code_utf16(endianness):
    endianness = endianness.lower()
    if endianness not in ('l', 'b', 'le', 'be', 'little', 'big'):
        raise ValueError("NFA_any_code_utf16: argument endianness must be one \
of 'L', 'LE', 'LITTLE', 'B', 'BE', or 'BIG', case insensitive, but got " + \
f'"{endianness}".')

    # UTF-16 any code machine
    q0, qF = State(), State()

    if endianness.lower() in ('b', 'be', 'big'):
        code_bytes = (_utf16_hi, _utf16_lo)
        hi_surrogate_bytes = (_utf16_hi_surrogate_hi, _utf16_surrogate_lo)
        lo_surrogate_bytes = (_utf16_lo_surrogate_hi, _utf16_surrogate_lo)
    else:
        code_bytes = (_utf16_lo, _utf16_hi)
        hi_surrogate_bytes = (_utf16_surrogate_lo, _utf16_hi_surrogate_hi)
        lo_surrogate_bytes = (_utf16_surrogate_lo, _utf16_lo_surrogate_hi)

    q2b0 = State()                               # two byte path states
    q4b0, q4b1, q4b2 = State(), State(), State() # four byte path states

    trans = machine.TxSet([
        # two byte encoding for 16 bit codepoint
        (  q0, q2b0, code_bytes[0]),
        (q2b0,   qF, code_bytes[1]),
        # four byte encoding for two 16 bit surrogates (code units)
        (  q0, q4b0, hi_surrogate_bytes[0]),
        (q4b0, q4b1, hi_surrogate_bytes[1]),
        (q4b1, q4b2, lo_surrogate_bytes[0]),
        (q4b2,   qF, lo_surrogate_bytes[1])
        ])

    return NFA(
        Q=common.set_of(q0, qF, q2b0, q4b0, q4b1, q4b2),
        trans=trans,
        q0=q0, F=common.set_of(qF),
        alphabet=alphabet._utf16_alphabet)

def NFA_any_code_utf32(endianness):
    endianness = endianness.lower()
    if endianness not in ('l', 'b', 'le', 'be', 'little', 'big'):
        raise ValueError("NFA_any_code_utf32: argument endianness must be one \
of 'L', 'LE', 'LITTLE', 'B', 'BE', or 'BIG', case insensitive, but got " + \
f'"{endianness}".')

    # UTF-16 any code machine
    q0, qF = State(), State()

    code_hi_byte = OrdinalSet([range(0x00, 0x11)])
    code_lo_byte = OrdinalSet([range(0x00, 0x100)])

    if endianness.lower() in ('b', 'be', 'big'):
        code_bytes = (code_hi_byte, code_lo_byte, code_lo_byte, code_lo_byte)
    else:
        code_bytes = (code_lo_byte, code_lo_byte, code_lo_byte, code_hi_byte)

    q4b0, q4b1, q4b2 = State(), State(), State() # four byte path states

    trans = machine.TxSet([
        # four byte encoding for 32-bit code points 
        (  q0, q4b0, code_bytes[0]),
        (q4b0, q4b1, code_bytes[1]),
        (q4b1, q4b2, code_bytes[2]),
        (q4b2,   qF, code_bytes[3])
        ])

    return NFA(
        Q=common.set_of(q0, qF, q4b0, q4b1, q4b2),
        trans=trans,
        q0=q0, F=common.set_of(qF),
        alphabet=alphabet._utf32_alphabet)

def NFA_any_code(encoding, endianness, minimize=True):
    encoding = encoding.lower()
    if encoding == 'unicode':
        M = NFA_any_code_unicode()
    elif encoding in ('asc', 'ascii'):
        M = NFA_any_code_ascii()
    elif encoding in ('utf-8', 'utf8'):
        M = NFA_any_code_utf8()
    elif encoding in ('utf-16', 'utf16'):
        M = NFA_any_code_utf16(endianness)
    elif encoding in ('utf-32', 'utf32'):
        M = NFA_any_code_utf32(endianness)
    else:
        raise ValueError(f'NFA_any_code: Unsupported encoding "{encoding}".')

    if minimize: 
        M = NFA_minimize(M)

    return M

def NFA_no_code(encoding, endianness, minimize=True):
    return NFA_negate(
        NFA_any_code(encoding, endianness, minimize=False),
        minimize=minimize)

def NFA_sym(a):
    q0, qF = State(), State()
    if not isinstance(a, abc.Iterable):
        a = (a,)
    symclass = OrdinalSet(a)
    return NFA(
        Q=common.set_of(q0, qF),
        trans=machine.TxSet([(q0, qF, symclass)]),
        q0=q0, F=common.set_of(qF),
        alphabet=symclass)

def NFA_sym_unicode(a, encoding, endianness):
    if isinstance(a, OrdinalSet):
        codes = a
    elif isinstance(a, int):
        codes = OrdinalSet((a,))
    elif isinstance(a, str):
        codes = OrdinalSet(a)
    elif isinstance(a, bytes):
        return NFA_bytestring(a)
    else:
        raise ValueError("NFA_sym_unicode: Argument must be an instance of \
OrdinalSet, int, str, or bytes.")

    ab = alphabet.code_alphabet_of(encoding)
    if not codes.issubset(ab):
        raise ValueError(
            f"NFA_sym: the codes {codes} are not a subset of " + \
            f"the encoding's ({encoding}) alphabet {ab}.")

    if len(codes) == 0:
        # this is no code, create an NFA that always rejects
        M = NFA_no_code(encoding, endianness)
    elif len(codes) == len(ab):
        # this is any code, create a NFA that always accepts
        M = NFA_any_code(encoding, endianness)
    elif len(codes) < len(ab) // 2:
        # it is more efficient to create the positive machine
        M = NFA_codes(codes, encoding, endianness)
    else:
        # it is more efficient to create the negative machine and subtract
        # it from the any code machine
        M = DFA_to_NFA(
            automata_difference([
                NFA_any_code(encoding, endianness),
                NFA_codes(ab - codes, encoding, endianness)
                ]))

    return automata_labeling(M, new_state_labels(M))

def _automata_setop(Ms, langtest, minimize):
    U, L_map = automata_list_to_DFPA(Ms, minimize=minimize)

    # perform difference
    F = []
    for q, langs in L_map.items():
        if langtest(langs, len(Ms)):
            F.append(q)

    return DFA(
        Q=U.Q, trans=U.trans,
        q0=U.q0, F=frozenset(F),
        alphabet=U.alphabet)

def _automata_union_f(langs, N):
    return any(i in langs for i in range(N))

def _automata_isect_f(langs, N):
    return all(i in langs for i in range(N))

def _automata_diff_f(langs, N):
    return 0 in langs and not any(i in langs for i in range(1, N))

def _automata_symdiff_f2(langs, N):
    if N != 2:
        raise ValueError("_automata_symdiff_f2: low level symmetric difference of automata is unsupported for N != 2.")
    return (0 in langs or 1 in langs) and not (0 in langs and 1 in langs)

def automata_union(Ms, minimize=True):
    return _automata_setop(Ms, _automata_union_f, minimize)

def automata_intersection(Ms, minimize=True):
    return _automata_setop(Ms, _automata_isect_f, minimize)

def automata_difference(Ms, minimize=True):
    return _automata_setop(Ms, _automata_diff_f, minimize)

def _automata_symmetric_difference_impl(M, N, minimize):
    return _automata_setop((M, N), _automata_symdiff_f2, minimize)

def automata_symmetric_difference(Ms, minimize=True):
    M = Ms[0]
    for N in Ms[1:]:
        M = _automata_symmetric_difference_impl(M, N, minimize)
    return M

def NFA_cat(M, N):
    if not (isinstance(M, NFA) and isinstance(N, NFA)):
        raise ValueError("NFA_cat must be passed two NFAs.")

    trans = M.trans.union(N.trans, ((qF, N.q0, EPSILON) for qF in M.F))
    return NFA(
        Q=M.Q | N.Q,
        trans=trans,
        q0=M.q0, F=N.F,
        alphabet=M.alphabet | N.alphabet)

def NFA_alt(M, N):
    if not (isinstance(M, NFA) and isinstance(N, NFA)):
        raise ValueError("NFA_alt must be passed two NFAs.")

    q0, qF = State(), State()
    trans_in = ((q0, M.q0, EPSILON), (q0, N.q0, EPSILON))
    trans_Mout = ((MqF, qF, EPSILON) for MqF in M.F)
    trans_Nout = ((NqF, qF, EPSILON) for NqF in N.F)
    trans = M.trans.union(trans_in, trans_Mout, trans_Nout, N.trans)
    return NFA(
        Q=common.union_of((q0, qF), M.Q, N.Q),
        trans=trans,
        q0=q0, F=common.set_of(qF),
        alphabet=M.alphabet | N.alphabet)

# the skip and repeat construction used for regular repetition, as well as the
# positive repetition (with no skip) and conditional (skippable, but no repeat)
# variants.
def _NFA_rep_impl(M, rep, skip, name):
    if not isinstance(M, NFA):
        raise ValueError(f"{name}: argument must be an NFA.")

    if rep or skip:
        q0, qF = State(), State()

        trans_in = [(q0, M.q0, EPSILON)]              # entry transition
        if skip: trans_in.append((q0, qF, EPSILON))   # skip transition
        if rep:  trans_in.append((qF, M.q0, EPSILON)) # repeat transition
    else:
        q0, qF = M.q0, State()
        trans_in = ()

    trans_outer = ((MqF, qF, EPSILON) for MqF in M.F) # final transitions
    trans = M.trans.union(trans_in, trans_outer)

    return NFA(
        Q=common.union_of((q0, qF), M.Q),
        trans=trans,
        q0=q0, F=common.set_of(qF),
        alphabet=M.alphabet)

# regular repetition
def NFA_rep(M):
    return _NFA_rep_impl(M, rep=True, skip=True, name="NFA_rep")

# positive repetition: repetition variant not having skip edge (inner machine
#                      must accept least once)
def NFA_repp(M):
    return _NFA_rep_impl(M, rep=True, skip=False, name="NFA_repp")

# conditional: repetition variant having skip edge but not repeat edge
def NFA_cond(M):
    return _NFA_rep_impl(M, rep=False, skip=True, name="NFA_cond")

# force an arbitrary NFA to be guaranteed compatible with the regular NFA
# constructions above by adding padding initial and final states.
def NFA_norm(M):
    if not isinstance(M, NFA):
        raise ValueError("NFA_norm: argument must be an NFA.")

    if len(M.F) > 1:
        return _NFA_rep_impl(M, rep=False, skip=False, name="NFA_norm")
    else:
        return M

# get the epsilon closure of a set of states S in automata M where S is a subset
# of M.Q. optionally, a transition map QE may be provided if it is already
# computed to prevent repeated constructions of the transition map, such as
# during the powerset construction when this must be called many times. if
# provided the transition map must include all epsilon transitions of the
# states accessible by epsilon transitions from the states in S. typically if
# QE is passed, it should be the complete transition map of M to guarantee
# correctness of the result.
def NFA_epsilon_closure(S, M, QE = None):
    if not isinstance(M, NFA):
        raise ValueError(
            "NFA_epsilon_closure must be passed a set of states and an NFA.")
 
    Q = deque()
    S = set(S)
    Q.extend(S)

    own_QE = QE is None
    if own_QE:
        QE = M.trans.transition_map(E=machine.EPSILON_SET)

    while Q:
        q = Q.pop()
        for r, E in QE[q].items():
            if machine.EPSILON_ORDINAL in E and r not in S:
                Q.append(r)
                S.add(r)

    if own_QE:
        QE.clear()
    
    return frozenset(S)

def automata_to_DFA(M, minimize=True):
    if isinstance(M, DFA):
        N = M
    elif isinstance(M, NFA):
        N = NFA_powerset_construction(M)
    else:
        raise ValueError("automata_to_DFA: argument must be a DFA or an NFA.")

    if minimize:
        N = DFA_minimize(N)

    return N

def NFA_to_DFA(M, minimize=True):
    if not isinstance(M, NFA):
        raise ValueError("NFA_to_DFA: argument must be an NFA.")

    N = NFA_powerset_construction(M)
    if minimize:
        N = DFA_minimize(N)

    return N

# transform an NFA in to an equivalent DFA using the powerset construction.
# this is an optimized version which avoids traversing all of the subsets of
# states so far discovered with every pass as is done with the simple
# transitive closure version of the algorithm. instead of this, we use a queue
# to feed the state sets to consider, and only push new sets in to the queue in
# the event that they haven't yet been considered so that redundant work is not
# performed in each pass.
def NFA_powerset_construction(M):
    if not isinstance(M, NFA):
        raise ValueError("NFA_powerset_construction must be passed an NFA.")

    QE = M.trans.transition_map()
    S = NFA_epsilon_closure(common.set_of(M.q0), M, QE)

    states = set((S,))
    final_states = set()
    trans = []

    queue = deque()
    queue.append(S)

    next_states = []
    error_state = None

    while queue:
        T = queue.popleft()

        if not M.F.isdisjoint(T):
            final_states.add(T)

        for q in T:
            for r, tx in QE[q].items():
                E = tx - machine.EPSILON_SET
                this_R = common.set_of(r)

                while E:
                    i = len(next_states) - 1
                    for e, _ in next_states[::-1]:
                        if not e.isdisjoint(E):
                            break
                        i -= 1

                    if i < 0:
                        next_states.append((E, this_R))
                        E = None
                    else:
                        e, e_R = next_states.pop(i)

                        shared = E & e
                        if e != shared:
                            next_states.append((e - shared, e_R))
                        next_states.append((shared, e_R | this_R))

                        E = E - shared

        unused_symbols = MutableOrdinalSet(M.alphabet)
        for E, R in next_states:
            unused_symbols.difference_update(E)
            U = NFA_epsilon_closure(R, M, QE)
            trans.append((T, U, E))
            if U not in states:
                states.add(U)
                queue.append(U)

        next_states.clear()

        if len(unused_symbols) > 0:
            if error_state is None:
                error_state = frozenset()
                states.add(error_state)
                trans.append((error_state, error_state, M.alphabet))
            trans.append((T, error_state, unused_symbols))

    return DFA(
        Q=frozenset(states),
        trans=machine.TxSet(trans),
        q0=S,
        F=frozenset(final_states),
        alphabet=M.alphabet)

def automata_negate(M, minimize=True, normalize=None):
    if isinstance(M, DFA):
        if normalize is not None:
            raise ValueError(
                "automata_negate: expected optional argument normalize to " + \
                f"be None when argument is not an NFA but got {normalize}.")
        return DFA_negate(M, minimize=minimize)
    elif isinstance(M, NFA):
        if normalize is None:
            normalize = True
        return NFA_negate(M, minimize=minimize, normalize=normalize)
    else:
        raise ValueError(
            "automata_negate: the argument must be a DFA or an NFA.")

# Negate the language accepted by an NFA. This consists of producing a DFA from
# the NFA, negating the DFA, and converting back to an NFA.
def NFA_negate(M, minimize=True, normalize=True):
    if not isinstance(M, NFA):
        raise ValueError("NFA_negate: the argument must be a NFA.")

    return DFA_to_NFA(
        DFA_negate(
            NFA_powerset_construction(M),
            minimize=minimize), normalize=normalize)

# Negate the language accepted by a DFA. This is a trivial operation consisting
# of copying the same DFA but setting the new final states to the difference
# between the set of states and the old final states. This makes every final
# state a non-final state and every non-final state a final state.
def DFA_negate(M, minimize=True):
    if not isinstance(M, DFA):
        raise ValueError("DFA_negate: the argument must be a DFA.")

    notM = DFA(Q=M.Q, trans=M.trans, q0=M.q0, F=M.Q-M.F, alphabet=M.alphabet)

    if minimize:
        notM = DFA_minimize(notM)

    return notM

def automata_to_NFA(M, normalize=True):
    if isinstance(M, NFA):
        N = M
        if normalize:
            N = NFA_norm(N)
    elif isinstance(M, DFA):
        N = DFA_to_NFA(M, normalize=normalize)
    else:
        raise ValueError("automata_to_NFA: argument must be a DFA or an NFA.")

    return N

# convert a DFA to an NFA. while technically nothing must be changed except for
# the nominal identification of automata type (since every DFA is an NFA
# without epsilon transitions), we create a single final state and add epsilon
# transitions from the final states of the DFA to the new final state in order
# to make it compatible with the NFA regular constructions as well as to make
# it easier to keep track of the final state since it forces there to be only
# one final state. this can be disabled by passing normalize=False.
def DFA_to_NFA(M, normalize=True):
    if not isinstance(M, DFA):
        raise ValueError("DFA_to_NFA must be passed a DFA.")

    N = NFA(Q=M.Q, trans=M.trans, q0=M.q0, F=M.F, alphabet=M.alphabet)

    if normalize:
        # epsilon transitions from final DFA states to single final NFA state
        return NFA_norm(N)
    else:
        # we are opting not to inject the single final state. while the input
        # DFA is a valid NFA, it is not an NFA that is compatible with the
        # regular NFA constructions.
        return N

def automata_minimize(M, normalize=True):
    if isinstance(M, DFA):
        return DFA_minimize(M)
    elif isinstance(M, NFA):
        return NFA_minimize(M, normalize=normalize)
    else:
        raise ValueError(
            "automata_minimize: argument must be a DFA or an NFA.")

def NFA_minimize(M, normalize=True):
    if not isinstance(M, NFA):
        raise ValueError("NFA_minimize: argument must be an NFA.")

    return DFA_to_NFA(NFA_to_DFA(M, minimize=True), normalize=normalize)

def DFA_minimize(M):
    if not isinstance(M, DFA):
        raise ValueError("DFA_minimize: argument must be a DFA.")

    return DFA_myhill_nerode_reduction(M, DFA_hopcroft_partition(M))

# create a Hopcroft partition which when collapsed to a new minimal DFA using
# DFA_myhill_nerode_reduction will satisfy the Myhill-Nerode relation. if
# initial_partition is specified, then sets of states other than the final and
# non-final states can be forced to be regarded as distinct a priori, for
# example in the case of combined automata that can specify which type of
# lexeme they're recognizing. it works because the Hopcroft algorithm only
# refines the existing partition, so whatever is definitely regarded as
# distinct in the initial state remains distinct in every subsequent iteration
# by. the proof is trivial by induction, and requires just a polylingual
# extension of DFAs and the Myhill-Nerode relation, where an automata may
# accept a number of distinct regular languages with at least one final state
# for each language that is unique from the others, so that entering in to it
# will not only cause the automata to accept but also to identify which
# language it has accepted a word from by way of a labeling of the final
# states.
def DFA_hopcroft_partition(M, initial_partition=None):
    if not isinstance(M, DFA):
        raise ValueError(
            "DFA_hopcroft_partition: \
argument must be a DFA and optionally an initial partition.")

    if initial_partition is None:
        # classic starting state, all final states regarded as not-inequivalent
        P = {M.F, M.Q - M.F} # output partition
        W = {M.F, M.Q - M.F} # work set (candidates for splitting)
    else:
        # caller defined starting state. useful for keeping the final states
        # separate a priori in case they represent different source languages
        P = set(initial_partition) # output partition
        W = set(P)                 # work set

    ER = M.trans.transition_map(reversed=True)
    tmp = set()

    while len(W) > 0:
        A = W.pop()
        for E, RQ in ER:
            # let X be the states for which 'E' leads to some state in A
            X = frozenset(q for r in A for q in (RQ[r] if r in RQ else ()))
            
            # for each Y in P
            while len(P) > 0:
                Y = P.pop()
                if X.isdisjoint(Y):
                    tmp.add(Y)
                    continue

                diff = Y - X
                if len(diff) == 0:
                    tmp.add(Y)
                    continue

                # we can refine Y because Y clearly overlaps X but is not
                # contained wholly by X. so we split it between the states that
                # do lead to A on 'E' and the states that don't lead to A on
                # 'E', since those can't possibly be equivalent.

                isect = X & Y
                tmp.add(isect)
                tmp.add(diff)
                # update the working set
                if Y in W:
                    W.remove(Y)
                    W.add(isect)
                    W.add(diff)
                elif len(isect) <= len(diff):
                    W.add(isect)
                else:
                    W.add(diff)

            # we've built the new version of P in tmp, so swap P and tmp and
            # clear out tmp
            P, tmp = tmp, P
            tmp.clear()

    return frozenset(P)

# given an automata M and a set of state P that is a partition of M.Q, collapse P
# in to states connected by the edges of the states in each element of P. every
# edge from any state in one element of the partition to a state in another
# element of the partition must be known a priori to all have the same label
# for the language accepted by the collapsed automata to be guaranteed
# unchanged. the partition produced by the Hopcroft implementation is one such
# partition, and such a partition in particular preserves the Myhill-Nerode
# relation.
def DFA_myhill_nerode_reduction(M, P, L_map = None):
    if not isinstance(M, DFA):
        raise ValueError(
            "DFA_myhill_nerode_reduction \
must be passed a DFA and a partition of it's states.")

    P = partition_unsort(P)
    trans = []

    for q, r, E in M.trans:
        Q, R = None, None
        for U in P:
            if q in U:
                Q = frozenset(U)
            if r in U:
                R = frozenset(U)
        if Q is None or R is None:
            raise RuntimeError(
                "DFA_myhill_nerode_partition: set P is not a partition of M.Q")
        trans.append((Q, R, E))

    for U in P:
        if M.q0 in U:
            q0 = frozenset(U)
            break

    F = set()
    if L_map is not None:
        L_map_m = dict()
    for U in P:
        for qF in M.F:
            if qF in U:
                F.add(U)
                if L_map is not None:
                    if U in L_map_m:
                        L_map_m[U] = tuple(sorted(common.union_of(L_map_m[U], L_map[qF])))
                    else:
                        L_map_m[U] = L_map[qF]

    m = DFA(
        Q=P, trans=machine.TxSet(trans),
        q0=q0, F=frozenset(F),
        alphabet=M.alphabet)

    if L_map is None:
        return m
    else:
        return m, L_map_m

# given an automata M and a dictionary with a bijective mapping states in M.Q
# on to a new set of states called a labeling, return a new automata of the
# same type in which M.Q is replaced by the new states and the states of every
# transition are updated as well.
#
#   for example, if labels[q] == y and labels[r] == z, then q is replaced by y
#   and r is replaced by z, and any transitions (q, r, a) are replaced by
#   (y, z, a).
def automata_labeling(M, labels):
    newQ = frozenset(labels[q] for q in M.Q)
    newF = frozenset(labels[q] for q in M.F)
    newTrans = machine.TxSet((labels[q], labels[r], e) for q, r, e in M.trans)
    return M.__class__(
        Q=newQ, trans=newTrans,
        q0=labels[M.q0],
        F=newF,
        alphabet=M.alphabet)

# retained for backwards compatibility with previous versions of this file.
# forwards it's arguments to automata_labeling but validates that M is an NFA.
def NFA_labeling(M, labels):
    if not isinstance(M, NFA):
        raise ValueError(
            "NFA_labeling called on object not an instance of NFA.")
    return automata_labeling(M, labels)

# retained for backwards compatibility with previous versions of this file.
# forwards it's arguments to automata_labeling but validates that M is a DFA.
def DFA_labeling(M, labels):
    if not isinstance(M, DFA):
        raise ValueError(
            "DFA_labeling called on object not an instance of DFA.")
    return automata_labeling(M, labels)

# given a partition of states P from some set of states Q = Union P and a
# dictionary with a bijective mapping states on to a new set of states called a
# labeling, return a new partition S of the set of states R where R is the
# labeling of Q using "labels" where S in R retains the structure of P in Q.
# this is useful, for example, if one creates a partition P of the states M.Q
# of some automata M, but then creates a labeling N of M, and wants to obtain
# the partition S of N.Q which is of the same form as P with respect to M.Q.
#
# for example, this exact operation is needed in the dump_automata routine
# whenever a DFA is passed along with the optional partition to draw by
# clustering the nodes in labeled boxes. first the display labeling is applied
# to the DFA so that its nodes are replaced in a sensible order with 'q0',
# 'q1', 'q2', ..., 'qErr', choosing the labels according to an order that
# results in a left to right layout that tries to position nodes according to
# the shortest path from the initial state (this is encapsulated in the
# automata_walk and automata_walk_labels routines). then, that same
# display labeling is applied to the partition of the DFA's states that was
# passed in as well, so that it becomes a partition of the display labeled
# version of the DFA and will correspond appropriately to it.
def partition_labeling(P, labels, sorted=True, set=False):
    part = []
    for X in P:
        part.append([])
        for q in X:
            part[-1].append(labels[q])
        if sorted:
            part[-1].sort()
        else:
            part[-1] = frozenset(part[-1])
    if set:
        part = frozenset(part)
    elif sorted:
        part.sort()
    return part

def partition_sort(P):
    part = []
    for X in P:
        part.append([])
        for q in X:
            part[-1].append(q)
        part[-1].sort()
    part.sort()
    return part

def partition_unsort(P):
    part = []
    for X in P:
        part.append([])
        for q in X:
            part[-1].append(q)
        part[-1] = frozenset(part[-1])
    return frozenset(part)

# given a language map of a DFPA and a labeling, replace the state keys of the
# language map according to the labels.
def language_map_labeling(L_map, labels):
    return { labels[q]: L_map[q] for q in L_map.keys() }

# given a pair of label mappings, collapse them so that if a[q] -> r and
# b[r] -> s, then the result c[q] -> s. given a chain of label mappings, fold
# this collapse from the right.
def telescope_labels(label_chain):
    if len(label_chain) < 1:
        return label_chain[0]

    first, *rest = label_chain
    nxt = telescope_labels(rest)
    newlabels = dict()
    for q in first.items():
        newlabels[q] = nxt[first[q]]
    return newlabels

def is_error_state(q, M, QE=None):
    # q is an error state if every transition from it leads back to itself, and
    # it has a transition for every symbol in the alphabet, and q is not a
    # final state.

    if q in M.F:
        # it's a final state, so it categorically cannot be an error state
        return False

    ownQE = QE is None
    if ownQE:
        QE = M.trans.transition_map_of((q,))

    for r, _ in QE[q].items():
        if r != q:
            # There is a way to reach another state from here. Not an error
            # state.
            return False

    if ownQE:
        QE.clear()

    # No escape!
    return True

# compose a set of automata in an alternation such that the final states of
# the composed automata are preserved. return an ordered partition of the final
# states that associates each with the automata they originated from.
def polyautomata(Ms):
    q0 = State()

    Ms = tuple(Ms)

    newQ = common.union_of((q0,), (q for M in Ms for q in M.Q))
    newTrans = machine.TxSet(chain(
        (t for M in Ms for t in M.trans),
        ((q0, M.q0, EPSILON) for M in Ms)
        ))
    newF = frozenset(qF for M in Ms for qF in M.F)

    N = NFA(
        Q=newQ,
        trans=newTrans,
        q0=q0, F=newF,
        alphabet=newTrans.alphabet - machine.EPSILON_SET)

    return N, tuple(M.F for M in Ms)

def DFPA_language_map(M, P):
    iP = tuple(enumerate(P))
    L_map = dict()
    for q in M.F:
        L_map[q] = tuple(sorted(i for u in q for i, Q in iP if u in Q))
    return L_map

def DFPA_initial_hopcroft_partition(M, L_map=None, P=None):
    if L_map is None:
        L_map = DFPA_language_map(M, P)

    initial = { M.Q - M.F }
    rmap = dict()
    for q, I in L_map.items():
        if I not in rmap:
            rmap[I] = set()
        rmap[I].add(q)

    return common.union_of((M.Q - M.F,), (frozenset(F) for F in rmap.values()))

def polyautomata_to_DFPA(N, P, minimize=True, L_map=None):
    M = NFA_powerset_construction(N)

    ownL_map = L_map is None
    if ownL_map:
        L_map = DFPA_language_map(M, P)

    if minimize:
        H = DFPA_initial_hopcroft_partition(M, L_map)
        J = DFA_hopcroft_partition(M, H)
        m, L_map_m = DFA_myhill_nerode_reduction(M, J, L_map)

        if ownL_map:
            L_map.clear()

        return m, L_map_m
    else:
        return M, L_map

def automata_list_to_DFPA(Ms, minimize=True):
    # the nth element of P is the set of final states of the
    # nth automata in Ms
    N, P = polyautomata(Ms)
    return polyautomata_to_DFPA(N, P, minimize=minimize)

# TODO : document the breadth first ranking system and emitter ordering choices
def L0_automata_walk(M):
    checkfinal = lambda q: q in M.F
    QE = M.trans.transition_map()

    visited = set()
    emitted = set()
    seen = set()

    heap = [(0, 0, M.q0)]
    edges = []
    final = []

    toplevel = -1
    while heap:
        level, _, q = heapq.heappop(heap)
        toplevel = max(level, toplevel)
        visited.add(q)

        yield 'RANK', level
        if checkfinal(q):
            if q is M.q0:
                yield 'STATE', q, 'FINAL'
            else:
                final.append(('STATE', q, 'FINAL'))
        else:
            yield 'STATE', q, 'NONFINAL'

        order = 0
        sort_key = lambda x: (x[1].lower_bound, x[1].upper_bound)
        for r, E in sorted(QE[q].items(), key=sort_key):
            if (q, r, E) not in emitted:
                if machine.EPSILON_ORDINAL in E:
                    edges.append(('TRANS', q, r, EPSILON))
                    diff = E - machine.EPSILON_SET
                    if diff:
                        edges.append(('TRANS', q, r, diff))
                else:
                    edges.append(('TRANS', q, r, E))
                emitted.add((q, r, E))

            if r not in visited and r not in seen:
                seen.add(r)
                heapq.heappush(heap, (level + 1, order, r))
                order += 1

        seen.clear()

    QE.clear()
    visited.clear()
    emitted.clear()

    yield 'RANK', toplevel + 1
    yield from final
    final.clear()

    yield from edges
    edges.clear()

# L1 collapses the rank information in to the state emissions
def L1_automata_walk(M):
    current_rank = None
    for mark, *data in L0_automata_walk(M):
        if mark == 'RANK':
            current_rank, = data
        elif mark == 'STATE':
            yield (mark, *data, current_rank)
        else:
            yield (mark, *data)

# L2 takes an ordered partition and accumulates the state information emitted
# by L1 in a structure, emitting only states from the first element of the
# ordered partition whose states have not yet all been emitted, saving the rest
# to be emitted as soon as no other elements before their element have states
# remaining to emit. in this way, the order in which L1 emits state information
# is preserved within each element of the partition, but the states in each
# element of the partition are emitted in a contiguous block, and those blocks
# are ordered according to the order of the partition. this is used to make
# it simple in dump_automata to write the states in each element of a partition
# in to a cluster, and is useful for batch processing of states in each element
# of a partition in breadth first walking order to the greatest extent possible
# in general.
def L2_automata_walk(M, P):
    tmp = tuple(set(Q) for Q in P)
    P, tmp = tmp, P
    del tmp

    W = [[] for _ in range(len(P))]

    depleted = []
    for mark, *data in L1_automata_walk(M):
        if mark == 'STATE':
            q, kind, level = data
            for i, Q in enumerate(P):
                if q in Q:
                    Q.remove(q)
                    W[i].append(('STATE', q, kind, level, i))
                    break
            for i, U in enumerate(W):
                if U is not None:
                    yield from U
                    U.clear()
                    if len(P[i]) == 0:
                        depleted.append(i)
                    else:
                        break
            if len(depleted) > 0:
                for i in depleted:
                    W[i] = None
                depleted.clear()
        else:
            yield (mark, *data)

# L3 takes the partition information from the state information emitted by L2
# and emits a change in which element of the partition is being output with the
# PART and END_PART marks by keeping track of the last partition element
# emitted, so that L3 output is the same as L1 output but with added emission
# of partition element delineators.
def L3_automata_walk(M, P):
    last_partition = None
    for mark, *data in L2_automata_walk(M, P):
        if mark == 'STATE':
            q, kind, level, partition = data
            if last_partition != partition:
                if last_partition is not None:
                    yield 'END_PART', last_partition
                yield 'PART', partition
                last_partition = partition
            yield 'STATE', q, kind, level
        elif mark == 'TRANS':
            if last_partition is not None:
                # we give all edges after all states, so this is safe
                yield 'END_PART', last_partition
                last_partition = None

            yield ('TRANS', *data)
        else:
            raise RuntimeError(f"Illegal mark at L3_automata_walk {mark}.")

def automata_walk(M, partition=None):
    if partition is None:
        partition = (M.Q,)
    yield from L3_automata_walk(M, partition)

def display_label_function(numeration=None, prefix=None, suffix=None, M=None, nonfinal=False):
    if numeration is None:
        iota = iter(count())
    else:
        iota = iter(numeration)
    
    def _wrap(_iota, _pfx, _sfx, _M, _QE, _nf):
        def display_label(*args):
            if _nf:
                q, level, partition = args
            else:
                q, kind, level, partition = args
            if _M is not None and is_error_state(q, _M, _QE):
                label = 'Err'
            else:
                label = _pfx + str(next(_iota)) + _sfx
            return label
        return display_label

    pfx = '' if prefix is None else prefix
    sfx = '' if suffix is None else suffix
    QE = M.trans.transition_map()
    return _wrap(iota, pfx, sfx, M, QE, nonfinal)

def DFPA_display_label_function(numeration=None, prefix=None, suffix=None, M=None, P=None, P_labels=None, L_map=None, nonfinal=False):
    f = display_label_function(
        numeration=numeration,
        M=M, nonfinal=True)

    def _wrap(_f, _pfx, _sfx, _M, _nf, _PL, _L, memo):
        def polyautomata_display_label(*args):
            if _nf:
                return _f(args)
            else:
                q, kind, level, partition = args
                if kind != 'FINAL':
                    return _f(q, level, partition)
                else:
                    langs = _L[q]
                    if langs not in memo:
                        memo[langs] = ['&bull;'.join(_PL[i] for i in _L[q]), 0]
                    result = '&#x2317;'.join(str(z) for z in memo[langs])
                    memo[langs][-1] += 1
                    return result
        return polyautomata_display_label

    pfx = '' if prefix is None else prefix
    sfx = '' if suffix is None else suffix
    if L_map is None:
        L_map = DFPA_language_map(M, P)

    final_memo = dict()
    return _wrap(f, pfx, sfx, M, nonfinal, P_labels, L_map, final_memo)

def automata_walk_labels(M, f, finalf=None, partition=None):
    if finalf is not None:
        g = lambda q, w, L, P: (finalf(q,L,P) if w == 'FINAL' else f(q,L,P))
    else:
        g = f

    labels = dict()
    current_partition = None
    for mark, *data in automata_walk(M, partition):
        if mark == 'PART':
            current_partition, = data
        elif mark == 'STATE':
            q, kind, level = data
            labels[q] = g(q, kind, level, current_partition)

    return labels

def new_state_labels(M):
    labels = dict()
    for q in M.Q:
        labels[q] = State()

    return labels

def subset_labels(M):
    labels = dict()
    for q in M.Q:
        labels[q] = f"{{{', '.join(sorted(f'{u}' for u in q))}}}"

    return labels

def partial_labels(M, labels):
    new_labels = dict()
    for q in M.Q:
        if q in labels:
            new_labels[q] = labels[q]
        else:
            new_labels[q] = q
    return new_labels

def dump_automata(M, f, partition=None, partition_labels=None, label_function=None, do_level_ranks=None, title=None, no_label=False):
    if do_level_ranks is None:
        do_level_ranks = False #isinstance(M, NFA)

    partition = tuple(partition) if partition is not None else (M.Q,)
    if partition_labels is None:
        partition_labels = tuple(str(x) for x in range(len(partition)))

    if not no_label:
        if label_function is None:
            label_function = display_label_function(M=M)

        labels = automata_walk_labels(M, label_function)
        partition = partition_labeling(partition, labels, sorted=False, set=False)
        M = automata_labeling(M, labels)

    print("digraph G {", file=f)
    print("  graph [rankdir=LR,ranksep=1.0,ordering=out", end="", file=f)
    if title is not None:
        if isinstance(title, bytes):
            title = title.decode('utf-8')
        elif not isinstance(title, str):
            title = str(title)
        print(f",label=<{title}>", end="", file=f)
    print("];", file=f)
    print("  node [shape=circle];", file=f)

    # entry point indicator
    print("  entry [shape=plaintext label=\"\"];", file=f)
    
    entity = lambda c: f"&#{hex(ord(c))[1:]};"
    def labelof(e):
        if e is EPSILON:
            return '&#x3b5;'
        else:
            return '<B>' + \
                ''.join(entity(x) for x in common.repr_symbol_bytes(e)) + \
                '</B>'

    edge_order = []
    edges = dict()
    nodenames = { 'entry': 'entry' }
    nodenamectr = 0

    if do_level_ranks:
        levels = { -1: ['entry'] }

    for mark, *data in automata_walk(M, partition):
        if mark == 'PART' and len(partition) > 1:
            part_i, = data
            print(f"  subgraph cluster_{part_i} {{", file=f)
        elif mark == 'END_PART' and len(partition) > 1:
            part_i, = data
            is_nonfinal = M.F.isdisjoint(partition[part_i])
            if partition_labels[part_i]:
                print(f"    label=<Q<SUB>{partition_labels[part_i]}</SUB>>;", file=f)
            else:
                print(f"    label=\"\";", file=f)
            print(f"    color={'black' if is_nonfinal else 'blue'};", file=f)
            print("  }", file=f)
        elif mark == 'STATE':
            q, kind, level = data
            if q not in nodenames:
                nodenames[q] = f"n{nodenamectr}"
                nodenamectr += 1
            label = f'label=<q<SUB>{q}</SUB>>]'
            if kind == 'FINAL':
                label = 'shape=doublecircle, ' + label
            label = '[' + label
            if len(partition) > 1:
                print("  ", end="", file=f)
            print(f"  {nodenames[q]} {label};", file=f) 
            if do_level_ranks:
                if level not in levels:
                    levels[level] = []
                levels[level].append(nodenames[q])
        elif mark == 'TRANS':
            q, r, E = data
            if (q, r) not in edges:
                edges[(q, r)] = []
                edge_order.append((q, r))
            edges[(q, r)].append(E)

    print(f"  entry -> {nodenames[M.q0]};", file=f);
    parts = []
    for q, r in edge_order:
        full_edge = MutableOrdinalSet()
        for x in edges[(q, r)]:
            if x is EPSILON:
                parts.append(labelof(EPSILON))
            else:
                if not full_edge.isdisjoint(x):
                    print(
                        f'WARNING: transition between {q} and {r}',
                        'specified in multiple OridnalSet',
                        'instances that are *not* disjoint!', file=sys.stderr)
                    print(
                        f'         {full_edge}, and {x}', file=sys.stderr)
                full_edge.update(x)

        if full_edge:
            parts.append(labelof(full_edge))

        print(f"  {nodenames[q]} -> {nodenames[r]} [label=<{', '.join(parts)}>]", file=f)
        parts.clear()

    edge_order.clear()
    edges.clear()

    if do_level_ranks:
        for level in sorted(levels.keys()):
            nodes = levels[level]
            print("  { rank=same; ", end="", file=f)
            for name in nodes:
                print(f"{name} ", end="", file=f)
            print("}", file=f)

    print("}", file=f)

