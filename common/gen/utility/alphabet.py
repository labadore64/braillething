# This is free and unencumbered software released into the public domain.
# 
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
# 
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
# 
# For more information, please refer to <https://unlicense.org>

# This is version 0.1 of the alphabet.py file in the Python 3 Practical Formal
# Parsing Library (PFPL), by @Forthegy#0149.
#
# If modifications are made and this file is redistributed, I kindly ask that
# you so state that your changes are distinct from the original.

from ordinal import OrdinalSet

_unicode_code_alphabet = OrdinalSet([range(0, 0x110000)])
_ascii_code_alphabet = OrdinalSet([range(0, 0x100)])
_utf8_code_alphabet  = _unicode_code_alphabet
_utf16_code_alphabet = _unicode_code_alphabet
_utf32_code_alphabet = _unicode_code_alphabet

_unicode_alphabet = _unicode_code_alphabet
_ascii_alphabet = _ascii_code_alphabet
_utf8_alphabet  = OrdinalSet([range(0, 0xF8)])
_utf16_alphabet = _ascii_code_alphabet
_utf32_alphabet = _ascii_code_alphabet

# alphabets of codepoints
def code_alphabet_of(encoding):
    encoding = encoding.lower()
    if encoding == 'unicode':
        return _unicode_code_alphabet
    elif encoding in ('asc', 'ascii'):
        return _ascii_code_alphabet
    elif encoding in ('utf-8', 'utf8'):
        return _utf8_code_alphabet
    elif encoding in ('utf-16', 'utf16'):
        return _utf16_code_alphabet
    elif encoding in ('utf-32', 'utf32'):
        return _utf32_code_alphabet
    else:
        raise ValueError(f'code_alphabet_of: Unsupported encoding "{encoding}".')

# alphabets of encodings
def byte_alphabet_of(encoding):
    encoding = encoding.lower()
    if encoding == 'unicode':
        return _unicode_alphabet
    elif encoding in ('asc', 'ascii'):
        return _ascii_alphabet
    elif encoding in ('utf-8', 'utf8'):
        return _utf8_alphabet
    elif encoding in ('utf-16', 'utf16'):
        return _utf16_alphabet
    elif encoding in ('utf-32', 'utf32'):
        return _utf32_alphabet
    else:
        raise ValueError(f'byte_alphabet_of: Unsupported encoding "{encoding}".')
