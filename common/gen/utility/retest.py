import sys, os, common, alphabet, regular, automata, fsm
from pathlib import Path

def mkdirp(dirname):
    path = Path(dirname).resolve()
    stk = []
    while not path.exists():
        stk.append(path.parts[-1])
        path = path.parent
    while stk:
        path = path / stk.pop()
        path.mkdir()

def fancy_heading(heading):
    if not isinstance(heading, str):
        heading = str(heading)
    print(f"o={'=' * len(heading)}=o")
    print(f"| {heading} |")
    print(f"o={'=' * len(heading)}=o")

def automata_test_driver(re, name, dirname,
        incremental=True, encoding=None, endianness=None, alphabet=None):

    mkdirp(dirname)
    results = []

    re_alphabet = regular.re_to_alphabet(re)
    if not re_alphabet.issubset(alphabet):
        diff = re_alphabet - alphabet
        print('skipping', name, 'because it cannot encode', dff)
        return

    ast, table = regular.re_to_AST(re,
        alphabet=alphabet,
        table=True)

    rpndumpname = f"{dirname}/rpn_{name}"
    astdumpname = f"{dirname}/ast_{name}"
    nfadumpname = f"{dirname}/nfa_{name}"
    dfadumpname = f"{dirname}/dfa_{name}"
    pardumpname = f"{dirname}/par_{name}"
    mindumpname = f"{dirname}/min_{name}"
    fsmdumpname = f"{dirname}/fsm_{name}"

    rpndump = f"{rpndumpname}.log"
    astdump = f"{astdumpname}.dot"
    nfadump = f"{nfadumpname}.dot"
    dfadump = f"{dfadumpname}.dot"
    pardump = f"{pardumpname}.dot"
    mindump = f"{mindumpname}.dot"
    fsmdump = f"{fsmdumpname}.log"

    rpndumplabel = f"RPN DUMP ({rpndump})"
    astdumplabel = f"AST DUMP ({astdump})"
    nfadumplabel = f"NFA DUMP ({nfadump})"
    dfadumplabel = f"DFA DUMP ({dfadump})"
    pardumplabel = f"PARTITIONED DFA DUMP ({pardump})"
    mindumplabel = f"MINIMAL DFA DUMP ({mindump})"
    fsmdumplabel = f"FINITE STATE MACHINE DUMP ({fsmdump})"

    with open(rpndump, 'w') as f:
        fancy_heading(rpndumplabel)
        common.dump_table(table, file=common.file_tee(sys.stdout, f))
    
    with open(astdump, 'w') as f:
        fancy_heading(astdumplabel)
        regular.dump_AST(ast,
            f, #common.file_tee(sys.stdout, f),
            title=name + ' AST')

    results.append((astdumpname, f'AST for {name}'))

    M = regular.AST_to_NFA(ast,
        incremental=incremental, encoding=encoding, endianness=endianness)

    if not incremental:
        with open(nfadump, 'w') as f:
            fancy_heading(nfadumplabel)
            automata.dump_automata(M,
                f, #common.file_tee(sys.stdout, f),
                title=name + ' NFA')

        results.append((nfadumpname, f'NFA for {name}'))

    M = automata.NFA_to_DFA(M, minimize=incremental)
    M = automata.automata_labeling(M, automata.new_state_labels(M))

    with open(dfadump, 'w') as f:
        fancy_heading(dfadumplabel)
        automata.dump_automata(M,
            f, #common.file_tee(sys.stdout, f),
            title=name + ' DFA')

    if incremental:
        results.append((dfadumpname, f'Incrementally produced DFA for {name}'))
    else:
        results.append((dfadumpname, f'DFA for {name}'))

    if not incremental:
        P = automata.DFA_hopcroft_partition(M)

        with open(pardump, 'w') as f:
            fancy_heading(pardumplabel)
            automata.dump_automata(M,
                f, #common.file_tee(sys.stdout, f),
                title=name + ' DFA Hopcroft partition',
                partition=P)

        results.append((pardumpname, f'DFA hopcroft partition for {name}'))

        M = automata.DFA_myhill_nerode_reduction(M, P)
        M = automata.automata_labeling(M, automata.new_state_labels(M))

        with open(mindump, 'w') as f:
            fancy_heading(mindumplabel)
            automata.dump_automata(mindfa,
                f, # common.file_tee(sys.stdout, f),
                title=name + ' minimal DFA')

        results.append((mindumpname, f'Minimal DFA for {name}'))

    spec = fsm.FSMSpecification(M)
    with open(fsmdump, 'w') as f:
        fancy_heading(fsmdumplabel)
        common.dump_table(
            spec.display_table,
            file=common.file_tee(sys.stdout, f))

    return results, spec

def automata_encoding_test(re, topname, dirname, incremental=True):
    encodings = [
        (topname + ' unicode',   None,     None,     'unicode', True),
        (topname + ' ascii',     'ascii',  None,     'ascii',   False),
        (topname + ' UTF-8',     'utf-8',  None,     'utf-8',   False),
        (topname + ' UTF-16-LE', 'utf-16', 'little', 'utf-16',  False),
        (topname + ' UTF-16-BE', 'utf-16', 'big',    'utf-16',  False),
        (topname + ' UTF-32-LE', 'utf-32', 'little', 'utf-32',  False),
        (topname + ' UTF-32-BE', 'utf-32', 'big',    'utf-32',  False)
        ]

    results = []
    fsm_out = None
    for name, enc, endian, alphabet_name, usemachine in encodings:
        ab = alphabet.code_alphabet_of(alphabet_name)
        subresults, fsm_spec = automata_test_driver(
            re, name, dirname, incremental, enc, endian, ab)
        results.extend(subresults)
        if usemachine:
            fsm_out = fsm.FSM(fsm_spec)

    mkdirp(dirname)
    
    for file, what in results:
        print("Render", what, '...')
        common.render_dot_pdf(file, what=what, verbose=True)

    print("Collating...")
    common.merge_pdfs(f'{dirname}/all_{topname}.pdf',
        [file + '.pdf' for file, _ in results],
        verbose=True)

    return fsm_out

if __name__ == '__main__':
    print('incremental? Y/n', end='>')
    incremental = input().lower() != 'n'
    try:
        while 1:
            re = input()
            if re != '':
                break
    except KeyboardInterrupt:
        exit()

    M = automata_encoding_test(re,
        'encoding test',
        'encodings_out',
        incremental=incremental)

    try:
        while 1:
            print('Input?>', end='')
            common.dump_table(M.run(input(), tabulate=True))
    except KeyboardInterrupt:
        print()

