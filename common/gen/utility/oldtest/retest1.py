import regular, automata, common

if __name__ == '__main__':
    def dump_re(title, stage, filename, T):
        with open(f"{filename}.dot", 'w') as f:
            regular.dump_AST(T, f, f"<B>{title}</B> - {filename} - <I>The {stage} Abstract Syntax Tree</I>")
        print(f" The graphvis description of the {stage} is in {filename}.dot.")

        if common.render_dot_pdf(filename, stage, verbose=True):
            return 'RESULT', True, stage, f'{filename}.dot', f'{filename}.pdf'
        else:
            return 'RESULT', False, stage, f'{filename}.dot', None

    def dump_machine(title, stage, filename, M, P=None, no_label=False, partition_labels=None):
        with open(f"{filename}.dot", 'w') as f:
            title = f"<B>{title}</B> - {filename} - <I>The {stage}</I>"
            automata.dump_automata(M, f, P, title=title, no_label=no_label, partition_labels=partition_labels)
        print(f" The graphvis description of the {stage} is in {filename}.dot.")

        if common.render_dot_pdf(filename, stage, verbose=True):
            return 'RESULT', True, stage, f'{filename}.dot', f'{filename}.pdf'
        else:
            return 'RESULT', False, stage, f'{filename}.dot', None
            
    def render_test_loop(inputs, incremental):
        for i, re in enumerate(inputs):
            rp, Q = regular.L2re(
                regular.L1re(regular.L0re(re)))
            regular.dump_L2re_table(rp, Q)

            title = common.to_hex_entities(re)

            result = regular.L3re(Q)
            yield dump_re(title, 'RPN AST', f'L3_{i}', result[0])

            expanded = regular.L4re(result[0])
            yield dump_re(title, 'expanded AST', f'L4_{i}', expanded)

            N = regular.AST_to_NFA(expanded, incremental=incremental)

            if not incremental:
                yield dump_machine(title, 'constructed NFA', f'NFA_{i}', N)

                M = automata.NFA_to_DFA(N)

                yield dump_machine(title,
                    'DFA from the powerset construction on the NFA',
                    f'DFA_{i}', M)

                P = automata.DFA_hopcroft_partition(M)

                yield dump_machine(title, 'Hopcroft partition refinement of the DFA',
                        f'PartDFA_{i}', M, P)

                m = automata.DFA_myhill_nerode_reduction(M, P)
            else:
                m = automata.NFA_to_DFA(N)

            if incremental:
                yield dump_machine(title,
                    'incrementally produced minimal DFA',
                    f'MinDFA_{i}', m)
            else:
                yield dump_machine(title,
                    'collapse of the Hopcroft partition to a minimal DFA',
                    f'MinDFA_{i}', m)

            yield ('MERGE', i, re, m)

        yield ('FINISH',)

    def render_test_1(inputs, incremental):
        acc = []
        outputs = []
        for action, *result in render_test_loop(inputs, incremental):
            if action == 'RESULT':
                acc.append(result)
            elif action == 'MERGE':
                i, re, M = result
                pdfs = [pdf for s, _, __, pdf in acc if s and isinstance(pdf, str)]

                if len(pdfs) > 0:
                    outfile = f"out{i}.pdf"
                    if common.merge_pdfs(outfile, pdfs, what=f"language {re}"):
                        outputs.append((outfile, re, M))
                else:
                    print("No PDF results to collate.")
                tab = [('', 'STAGE', 'DOT OUTPUT', 'PDF OUTPUT')]
                for success, stage, dot, pdf in acc:
                    check = ' \u2713' if success else ' \u2717'
                    pdfresult = pdf if success else '-'
                    tab.append((check, f"The {stage}.", dot, pdfresult))
                common.dump_table(tab, padding=1)
                acc.clear()
            elif action == 'FINISH':
                print(f"There are {len(outputs)} machine input(s).")
                if len(outputs) > 0:
                    outfile = f"all.pdf"
                    infiles = [name for name, re, M in outputs]

                    if incremental:
                        # we'll do the special multilanguage alternation test
                        names = [name for name, _, __ in outputs]
                        res = [re for _, re, __ in outputs]
                        Ms = [M for _, __, M in outputs]

                        # the nth element of P is the set of final states of the
                        # nth automata in Ms
                        N, P = automata.polyautomata(Ms)

                        labelP = tuple(common.to_hex_entities(re) for re in res)
                        labelP = ('',) + labelP
                        _, s, __, dot, pdf = dump_machine(
                            "Master NFA",
                            "composition of the DFAs in to an NFA taken together with a language partition",
                            "MasterNFA", N, (N.Q - N.F,) + P,
                            partition_labels=labelP)
                        if s:
                            infiles.append(pdf)

                        M = automata.NFA_to_DFA(N)
                        L_map = automata.DFPA_language_map(M, P)

                        display_labels = automata.automata_walk_labels(
                            M, automata.DFPA_display_label_function(
                                M=M, P_labels=labelP[1:], L_map=L_map))

                        displayM = automata.automata_labeling(M, display_labels)

                        _, s, __, dot, pdf = dump_machine(
                            "Master DFPA",
                            "DFPA from the subset construction of the NFA taken together with the language partition",
                            "MasterDFPA", displayM, no_label=True)
                        if s:
                            infiles.append(pdf)

                        H = automata.DFPA_initial_hopcroft_partition(M, L_map)
                        displayH = automata.partition_labeling(H, display_labels)

                        _, s, __, dot, pdf = dump_machine(
                            "Master DFPA Coarse Partition",
                            "master DFPA multilingual coarse partition",
                            "CoarsePartMasterDFPA", displayM, displayH, no_label=True)
                        if s:
                            infiles.append(pdf)

                        J = automata.DFA_hopcroft_partition(M, H)
                        displayJ = automata.partition_labeling(J, display_labels)

                        _, s, __, dot, pdf = dump_machine(
                            "Master DFPA Refined Partition",
                            "master DFPA multilingual refined partition",
                            "RefinedPartMasterDFPA", displayM, displayJ, no_label=True)
                        if s:
                            infiles.append(pdf)

                        m, L_map_m = automata.DFA_myhill_nerode_reduction(M, J, L_map)

                        display_labels_m = automata.automata_walk_labels(
                            m, automata.DFPA_display_label_function(
                                M=m, P_labels=labelP[1:], L_map=L_map_m))

                        display_m = automata.automata_labeling(m, display_labels_m)

                        _, s, __, dot, pdf = dump_machine(
                            "Minimal Master DFPA",
                            "minimized master DFPA after Myhill-Nerode reduction",
                            "MinMasterDFPA", display_m, no_label=True)
                        if s:
                            infiles.append(pdf)

                    common.merge_pdfs(outfile, infiles, what=f"{len(outputs)} result(s)")
        print("STOP.")

    def input_loop():
        while 1:
            print("regular?> ", end="")
            try:
                re = input()
                if re != '':
                    yield re
                else:
                    break
            except KeyboardInterrupt:
                break

    render_test_1(input_loop(), incremental=True)

