import regular, automata, common

if __name__ is '__main__':
    res = []
    try:
        while 1:
            re = input()
            if re == '':
                break
            res.append(re)
    except KeyboardInterrupt:
        pass

    M, L_map = regular.res_to_DFPA(res)

    lang_labels = [common.to_hex_entities(re) for re in res]
    display_labels = automata.automata_walk_labels(
        M, automata.DFPA_display_label_function(
            M=M, P_labels=lang_labels, L_map=L_map))
    
    displayM = automata.automata_labeling(M, display_labels)
    with open("result.dot", 'w') as f:
        automata.dump_automata(displayM, f,
            no_label=True, title="&bull;".join(lang_labels))

    bullet = '\u2022' 
    common.render_dot_pdf("result",
        f"languages {f'{bullet}'.join(repr(re) for re in res)}", verbose=True)

